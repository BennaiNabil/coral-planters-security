#!/bin/bash

# Exécutez la commande Maven pour générer le rapport Surefire
./mvnw surefire-report:report

# Vérifiez si la commande a réussi
if [ $? -eq 0 ]; then
    # Ouvrez le rapport généré dans le navigateur par défaut
    if [[ "$OSTYPE" == "darwin"* ]]; then
        open target/site/surefire-report.html
    elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
        xdg-open target/site/surefire-report.html
    elif [[ "$OSTYPE" == "cygwin" ]]; then
        cygstart target/site/surefire-report.html
    elif [[ "$OSTYPE" == "msys" ]]; then
        start target/site/surefire-report.html
    else
        echo "Impossible de détecter le système d'exploitation"
        exit 1
    fi
else
    echo "Erreur lors de la génération du rapport Surefire"
    exit 1
fi
