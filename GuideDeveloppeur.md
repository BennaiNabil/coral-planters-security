# Guide du développeur sur Coral Planters

<!-- TOC -->

* [Guide du développeur sur Coral Planters](#guide-du-développeur-sur-coral-planters)
    * [Prérequis](#prérequis)
    * [Modèle conceptuel de données](#modèle-conceptuel-de-données)
    * [Modèle physique de données](#modèle-physique-de-données)
    * [Architecture de l'API](#architecture-de-lapi)
    * [Conventions de l'API](#conventions-de-lapi)
        * [Packages](#packages)
        * [Entités JPA](#entités-jpa)
        * [JPA Repositories](#jpa-repositories)
        * [Services](#services)
        * [Contrôleurs](#contrôleurs)
        * [Processors](#processors)

<!-- TOC -->

## Prérequis

- Avoir lu le [README](README.md) du projet
- Avoir installé son environnement de développement
- Avoir installé les dépendances du projet
- Avoir testé son environnement de développement

## Modèle conceptuel de données

![](./documents/images/mcd.png)

## Modèle physique de données

![](./documents/images/mpd.png)


## Architecture de l'API

Notre API suit une logique de traitement de requêtes : un client envoie une demande à une URL de l'API, qui est
acheminée vers une méthode du contrôleur correspondant. N'oubliez pas l'annotation @RestController pour les contrôleurs.
Ensuite, la méthode du contrôleur appelle un bean de service pour traiter les données. N'oubliez pas l'annotation
@Service pour les services. Le contrôleur n'a pas de logique, il est simplement responsable du routage. Si nécessaire,
la méthode du service appelle un bean de traitement (processor), car le service ne gère pas la logique métier. N'oubliez
pas l'annotation @Component pour les processors. Une fois que le bean de traitement a effectué sa tâche, il transmet les
données traitées au service. Ensuite, le service appelle un bean de repository (étendant JpaRepository), qui est
responsable des échanges avec la base de données/couche de persistence. Si nécessaire, les données retournent du
repository au service, puis du service au contrôleur, puis du contrôleur au client, qui dans notre cas sera Angular.

![Diagramme de séquence de l'architecture de l'API](documents/images/diag_sequence_archi_api.png)

## Conventions de l'API

### Packages

- Les packages de l'API seront nommés en minuscules (ex: `org.simplon.coralplanters`)
- Les packages de l'API seront nommés en anglais (ex: `org.simplon.coralplanters.controllers`)

### Entités JPA

- Les entités JPA sont annotées avec ```@Entity``` pour indiquer qu'il s'agit d'une entité JPA.
- Les entités JPA sont annotées avec ```@Table``` pour indiquer qu'elles sont mappées à une table de base de données.
- Les entités JPA ont un constructeur sans argument et un constructeur qui prend tous les champs en tant qu'arguments,
  ce qui est une convention pour les entités JPA.
- Les entités JPA ont des getters et des setters pour tous leurs champs, ce qui est également une convention pour les
  entités JPA.
- Les entités JPA ont un champ annoté avec ```@Id``` pour indiquer qu'il est mappé à la clé primaire de la table.
- Le champ annoté avec ```@Id``` a une annotation ```@GeneratedValue``` correspondante pour indiquer que la base de
  données doit
  générer les valeurs de clé primaire automatiquement.
- Chaque champ est annoté avec ```@Column``` pour indiquer son mappage à une colonne de base de données.
- Les entités JPA remplacent ```toString()``` pour fournir une représentation sous forme de chaîne de l'objet à des fins
  de
  débogage.
- Les entités JPA utilisent les annotations de
  Lombok (```@AllArgsConstructor```, ```@NoArgsConstructor```, ```@Getter```, ```@Setter```,
  ```@ToString```) pour générer automatiquement les constructeurs, les getters, les setters et la
  méthode ```toString()```.

### JPA Repositories

- Annotation ```@Repository``` nécessaire
- Doit étendre l'interface ```JpaRepository<T, ID>```
- Convention de nommage des méthodes basée sur les noms des champs de l'entité
- Possibilité de personnaliser les méthodes avec une requête JPQL personnalisée en utilisant ```@Query```.

### Services

- Annotation ```@Service``` nécessaire
- Doit être annoté avec ```@Transactional``` pour indiquer que les méthodes du service doivent être exécutées dans
  une transaction. (une transaction est un ensemble d'instructions qui doivent être exécutées comme une unité)

### Contrôleurs

- Annotation ```@RestController``` nécessaire
- Doit être annoté avec ```@RequestMapping``` pour indiquer le chemin de base de l'API.
- Doit être annoté avec ```@CrossOrigin``` pour indiquer les origines autorisées pour les requêtes CORS.
- ```@Valid``` pour indiquer que les paramètres de la méthode doivent être validés.
- ```@RequestBody``` pour indiquer que le paramètre de la méthode doit être mappé à la requête HTTP.
- ```@PathVariable``` pour indiquer que le paramètre de la méthode doit être mappé à une variable de chemin dans la
  requête HTTP.
- ```@ResponseStatus``` pour indiquer le code de statut HTTP à renvoyer.

### Processors

- Annotation ```@Component``` nécessaire
- 
