--
-- PostgreSQL database dump
--

-- Dumped from database version 15.2 (Debian 15.2-1.pgdg110+1)
-- Dumped by pg_dump version 15.2 (Debian 15.2-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.SET_CONFIG('search_path', '', FALSE);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

SET SCHEMA 'public';

--
-- Name: 'group'; Type: TABLE; Schema: public; Owner: coralplanters
--

CREATE TABLE "'group'" (
    idgroup      BIGINT                         NOT NULL,
    closedat     TIMESTAMP(6) WITHOUT TIME ZONE,
    createdat    TIMESTAMP(6) WITHOUT TIME ZONE NOT NULL,
    name         CHARACTER VARYING(255)         NOT NULL,
    admin_id     BIGINT,
    creator_id   BIGINT,
    structure_id BIGINT);


ALTER TABLE "'group'"
    OWNER TO coralplanters;

--
-- Name: 'group'_idgroup_seq; Type: SEQUENCE; Schema: public; Owner: coralplanters
--

CREATE
    SEQUENCE "'group'_idgroup_seq"
    START
        WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE
        1;


ALTER TABLE "'group'_idgroup_seq"
    OWNER TO coralplanters;

--
-- Name: 'group'_idgroup_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coralplanters
--

ALTER
    SEQUENCE "'group'_idgroup_seq" OWNED BY "'group'".idgroup;


--
-- Name: 'user'; Type: TABLE; Schema: public; Owner: coralplanters
--

CREATE TABLE "'user'" (
    iduser      BIGINT                         NOT NULL,
    bannedat    TIMESTAMP(6) WITHOUT TIME ZONE,
    createdat   TIMESTAMP(6) WITHOUT TIME ZONE NOT NULL,
    email       CHARACTER VARYING(255)         NOT NULL,
    firstname   CHARACTER VARYING(255)         NOT NULL,
    lastname    CHARACTER VARYING(255)         NOT NULL,
    password    CHARACTER VARYING(255)         NOT NULL,
    phonenumber CHARACTER VARYING(255)         NOT NULL,
    role_id     BIGINT);


ALTER TABLE "'user'"
    OWNER TO coralplanters;

--
-- Name: 'user'_iduser_seq; Type: SEQUENCE; Schema: public; Owner: coralplanters
--

CREATE
    SEQUENCE "'user'_iduser_seq"
    START
        WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE
        1;


ALTER TABLE "'user'_iduser_seq"
    OWNER TO coralplanters;

--
-- Name: 'user'_iduser_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coralplanters
--

ALTER
    SEQUENCE "'user'_iduser_seq" OWNED BY "'user'".iduser;


--
-- Name: address; Type: TABLE; Schema: public; Owner: coralplanters
--

CREATE TABLE address (
    idaddress        BIGINT                 NOT NULL,
    address          CHARACTER VARYING(255) NOT NULL,
    city             CHARACTER VARYING(255) NOT NULL,
    country          CHARACTER VARYING(255) NOT NULL,
    isprimaryaddress BOOLEAN                NOT NULL,
    latitude         DOUBLE PRECISION       NOT NULL,
    longitude        DOUBLE PRECISION       NOT NULL,
    zipcode          CHARACTER VARYING(255) NOT NULL);


ALTER TABLE address
    OWNER TO coralplanters;

--
-- Name: address_idaddress_seq; Type: SEQUENCE; Schema: public; Owner: coralplanters
--

CREATE
    SEQUENCE address_idaddress_seq
    START
        WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE
        1;


ALTER TABLE address_idaddress_seq
    OWNER TO coralplanters;

--
-- Name: address_idaddress_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coralplanters
--

ALTER
    SEQUENCE address_idaddress_seq OWNED BY address.idaddress;


--
-- Name: bankaccount; Type: TABLE; Schema: public; Owner: coralplanters
--

CREATE TABLE bankaccount (
    idbankaccount BIGINT                 NOT NULL,
    accountnumber CHARACTER VARYING(255) NOT NULL,
    bankname      CHARACTER VARYING(255) NOT NULL,
    biccode       CHARACTER VARYING(255) NOT NULL,
    ibancode      CHARACTER VARYING(255) NOT NULL,
    user_id       BIGINT);


ALTER TABLE bankaccount
    OWNER TO coralplanters;

--
-- Name: bankaccount_idbankaccount_seq; Type: SEQUENCE; Schema: public; Owner: coralplanters
--

CREATE
    SEQUENCE bankaccount_idbankaccount_seq
    START
        WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE
        1;


ALTER TABLE bankaccount_idbankaccount_seq
    OWNER TO coralplanters;

--
-- Name: bankaccount_idbankaccount_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coralplanters
--

ALTER
    SEQUENCE bankaccount_idbankaccount_seq OWNED BY bankaccount.idbankaccount;


--
-- Name: coral; Type: TABLE; Schema: public; Owner: coralplanters
--

CREATE TABLE coral (
    idcoral         BIGINT                 NOT NULL,
    description     CHARACTER VARYING(255) NOT NULL,
    isparcel        BOOLEAN                NOT NULL,
    name            CHARACTER VARYING(255) NOT NULL,
    nature          CHARACTER VARYING(255) NOT NULL,
    address_id      BIGINT,
    coral_parent_id BIGINT);


ALTER TABLE coral
    OWNER TO coralplanters;

--
-- Name: coral_idcoral_seq; Type: SEQUENCE; Schema: public; Owner: coralplanters
--

CREATE
    SEQUENCE coral_idcoral_seq
    START
        WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE
        1;


ALTER TABLE coral_idcoral_seq
    OWNER TO coralplanters;

--
-- Name: coral_idcoral_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coralplanters
--

ALTER
    SEQUENCE coral_idcoral_seq OWNED BY coral.idcoral;


--
-- Name: group_user; Type: TABLE; Schema: public; Owner: coralplanters
--

CREATE TABLE group_user (
    group_id BIGINT NOT NULL,
    user_id  BIGINT NOT NULL);


ALTER TABLE group_user
    OWNER TO coralplanters;

--
-- Name: resource; Type: TABLE; Schema: public; Owner: coralplanters
--

CREATE TABLE resource (
    idresource      BIGINT                         NOT NULL,
    uploadedat      TIMESTAMP(6) WITHOUT TIME ZONE NOT NULL,
    url             CHARACTER VARYING(255)         NOT NULL,
    coral_id        BIGINT,
    resourcetype_id BIGINT);


ALTER TABLE resource
    OWNER TO coralplanters;

--
-- Name: resource_idresource_seq; Type: SEQUENCE; Schema: public; Owner: coralplanters
--

CREATE
    SEQUENCE resource_idresource_seq
    START
        WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE
        1;


ALTER TABLE resource_idresource_seq
    OWNER TO coralplanters;

--
-- Name: resource_idresource_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coralplanters
--

ALTER
    SEQUENCE resource_idresource_seq OWNED BY resource.idresource;


--
-- Name: resourcetype; Type: TABLE; Schema: public; Owner: coralplanters
--

CREATE TABLE resourcetype (
    idresourcetype BIGINT                 NOT NULL,
    resourcetype   CHARACTER VARYING(255) NOT NULL);


ALTER TABLE resourcetype
    OWNER TO coralplanters;

--
-- Name: resourcetype_idresourcetype_seq; Type: SEQUENCE; Schema: public; Owner: coralplanters
--

CREATE
    SEQUENCE resourcetype_idresourcetype_seq
    START
        WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE
        1;


ALTER TABLE resourcetype_idresourcetype_seq
    OWNER TO coralplanters;

--
-- Name: resourcetype_idresourcetype_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coralplanters
--

ALTER
    SEQUENCE resourcetype_idresourcetype_seq OWNED BY resourcetype.idresourcetype;


--
-- Name: role; Type: TABLE; Schema: public; Owner: coralplanters
--

CREATE TABLE role (
    idrole BIGINT                 NOT NULL,
    role   CHARACTER VARYING(255) NOT NULL);


ALTER TABLE role
    OWNER TO coralplanters;

--
-- Name: role_idrole_seq; Type: SEQUENCE; Schema: public; Owner: coralplanters
--

CREATE
    SEQUENCE role_idrole_seq
    START
        WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE
        1;


ALTER TABLE role_idrole_seq
    OWNER TO coralplanters;

--
-- Name: role_idrole_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coralplanters
--

ALTER
    SEQUENCE role_idrole_seq OWNED BY role.idrole;


--
-- Name: sponsorship; Type: TABLE; Schema: public; Owner: coralplanters
--

CREATE TABLE sponsorship (
    idsponsorship BIGINT                         NOT NULL,
    amount        INTEGER                        NOT NULL,
    startedat     TIMESTAMP(6) WITHOUT TIME ZONE NOT NULL,
    coral_id      BIGINT,
    sponsor_id    BIGINT);


ALTER TABLE sponsorship
    OWNER TO coralplanters;

--
-- Name: sponsorship_idsponsorship_seq; Type: SEQUENCE; Schema: public; Owner: coralplanters
--

CREATE
    SEQUENCE sponsorship_idsponsorship_seq
    START
        WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE
        1;


ALTER TABLE sponsorship_idsponsorship_seq
    OWNER TO coralplanters;

--
-- Name: sponsorship_idsponsorship_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coralplanters
--

ALTER
    SEQUENCE sponsorship_idsponsorship_seq OWNED BY sponsorship.idsponsorship;


--
-- Name: structure; Type: TABLE; Schema: public; Owner: coralplanters
--

CREATE TABLE structure (
    idstructure BIGINT                 NOT NULL,
    name        CHARACTER VARYING(255) NOT NULL,
    address_id  BIGINT);


ALTER TABLE structure
    OWNER TO coralplanters;

--
-- Name: structure_idstructure_seq; Type: SEQUENCE; Schema: public; Owner: coralplanters
--

CREATE
    SEQUENCE structure_idstructure_seq
    START
        WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE
        1;


ALTER TABLE structure_idstructure_seq
    OWNER TO coralplanters;

--
-- Name: structure_idstructure_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coralplanters
--

ALTER
    SEQUENCE structure_idstructure_seq OWNED BY structure.idstructure;


--
-- Name: user_address; Type: TABLE; Schema: public; Owner: coralplanters
--

CREATE TABLE user_address (
    user_id    BIGINT NOT NULL,
    address_id BIGINT NOT NULL);


ALTER TABLE user_address
    OWNER TO coralplanters;

--
-- Name: 'group' idgroup; Type: DEFAULT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY "'group'"
    ALTER
        COLUMN idgroup
        SET DEFAULT NEXTVAL('"''group''_idgroup_seq"'::regclass);


--
-- Name: 'user' iduser; Type: DEFAULT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY "'user'"
    ALTER
        COLUMN iduser
        SET DEFAULT NEXTVAL('"''user''_iduser_seq"'::regclass);


--
-- Name: address idaddress; Type: DEFAULT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY address
    ALTER
        COLUMN idaddress
        SET DEFAULT NEXTVAL('address_idaddress_seq'::regclass);


--
-- Name: bankaccount idbankaccount; Type: DEFAULT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY bankaccount
    ALTER
        COLUMN idbankaccount
        SET DEFAULT NEXTVAL('bankaccount_idbankaccount_seq'::regclass);


--
-- Name: coral idcoral; Type: DEFAULT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY coral
    ALTER
        COLUMN idcoral
        SET DEFAULT NEXTVAL('coral_idcoral_seq'::regclass);


--
-- Name: resource idresource; Type: DEFAULT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY resource
    ALTER
        COLUMN idresource
        SET DEFAULT NEXTVAL('resource_idresource_seq'::regclass);


--
-- Name: resourcetype idresourcetype; Type: DEFAULT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY resourcetype
    ALTER
        COLUMN idresourcetype
        SET DEFAULT NEXTVAL('resourcetype_idresourcetype_seq'::regclass);


--
-- Name: role idrole; Type: DEFAULT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY role
    ALTER
        COLUMN idrole
        SET DEFAULT NEXTVAL('role_idrole_seq'::regclass);


--
-- Name: sponsorship idsponsorship; Type: DEFAULT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY sponsorship
    ALTER
        COLUMN idsponsorship
        SET DEFAULT NEXTVAL('sponsorship_idsponsorship_seq'::regclass);


--
-- Name: structure idstructure; Type: DEFAULT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY structure
    ALTER
        COLUMN idstructure
        SET DEFAULT NEXTVAL('structure_idstructure_seq'::regclass);


--
-- Data for Name: 'group'; Type: TABLE DATA; Schema: public; Owner: coralplanters
--

INSERT INTO "'group'"
VALUES (1, NULL, '1993-08-26 08:33:44.856', 'Aerodynamic Paper Shirt', 3, 5, 4),
       (2, '1985-06-19 16:00:55.883', '1999-12-31 12:11:46.701', 'Practical Aluminum Bottle', 3, 4, 4),
       (3, '1963-05-28 13:16:37.898', '1965-04-28 10:23:28.619', 'Heavy Duty Plastic Watch', 6, 9, 3),
       (4, '1962-07-02 15:39:28.037', '1995-11-26 21:54:04.003', 'Synergistic Granite Table', 7, 2, 4),
       (5, '1993-01-26 13:06:39.756', '2003-01-22 17:05:11.408', 'Awesome Paper Bench', 6, 8, 4);


--
-- Data for Name: 'user'; Type: TABLE DATA; Schema: public; Owner: coralplanters
--

INSERT INTO "'user'"
VALUES (1, '1982-10-01 11:28:03.047', '1998-04-10 03:12:25.844', 'lina.mercier@gmail.com', 'Lina', 'Duval',
        'bclnl1xfum4s6s', '+33 5 81 36 95 62', 2),
       (2, NULL, '2001-07-05 15:48:45.415', 'lilou.richard@gmail.com', 'Marie', 'Joly', 'kljb6ldwu3y4s',
        '+33 504617463', 1),
       (3, NULL, '1981-01-29 18:11:23.672', 'jules.masson@yahoo.fr', 'Benjamin', 'Louis', 's4nvzg1t', '02 65 90 65 39',
        2),
       (4, NULL, '1990-07-19 03:36:57.356', 'alexandre.laurent@hotmail.fr', 'Evan', 'Jacquet', '667u7k74e3xho2p',
        '0395224672', 2),
       (5, NULL, '1984-06-13 12:23:40.576', 'lucas.blanchard@gmail.com', 'Thomas', 'Barre', 'esb4ob0xr1tqr',
        '02 42 26 58 09', 1),
       (6, NULL, '1987-10-09 18:14:42.483', 'eva.vidal@gmail.com', 'Sarah', 'Roux', 'b9yfcjemry23h',
        '+33 2 84 77 60 99', 1),
       (7, NULL, '2002-06-26 15:28:36.889', 'lina.cousin@yahoo.fr', 'Noah', 'Méunier', 'dlexkpv3k', '+33 5 45 61 47 26',
        2),
       (8, NULL, '1964-09-17 03:15:17.32', 'lucie.julien@gmail.com', 'Julie', 'Muller', '1g53to3u6', '0995196662', 1),
       (9, NULL, '1992-08-19 08:01:30.398', 'pauline.julien@gmail.com', 'Gabriel', 'Lacroix', 'lc85ne7znw',
        '+33 344824993', 2),
       (10, '1982-12-18 19:05:37.481', '1976-06-18 21:35:14.2', 'maeva.leveque@gmail.com', 'Maeva', 'Philippe',
        'g8ofdqzjifz', '03 18 65 18 35', 2);


--
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: coralplanters
--

INSERT INTO address
VALUES (1, '6 Passage Molière', 'Orléans', 'Somalia', FALSE, 31.940311, -131.82403, '54506'),
       (2, '3088 Place du Dahomey', 'Montauban', 'Vietnam', FALSE, -29.798717, 133.69921, '88082'),
       (3, '93 Rue de Provence', 'Dunkerque14', 'Kenya', TRUE, 70.698332, 31.671981, '47387'),
       (4, '4 Allée, Voie Saint-Bernard', 'Orléans', 'Wallis and Futuna', TRUE, -38.014324, 121.9082, '30632'),
       (5, '810 Passage Pastourelle', 'Nice', 'Pakistan', FALSE, -31.763102, 96.657718, '65017'),
       (6, '7 Impasse Saint-Honoré', 'Pessac', 'Tonga', TRUE, -42.056018, 95.77638, '96561'),
       (7, '5342 Boulevard Saint-Honoré', 'Saint-Étienne', 'Fiji', TRUE, 10.584846, -158.91301, '14075'),
       (8, '99 Allée, Voie Lepic', 'Le Havre', 'Kazakhstan', TRUE, -1.2810486, -127.7145, '59978'),
       (9, '6 Passage de Vaugirard', 'Troyes', 'Haiti', TRUE, 4.9655741, -40.684253, '33933'),
       (10, '500 Allée, Voie de Provence', 'Rueil-Malmaison', 'Belarus', FALSE, -22.494284, -70.648417, '84197');


--
-- Data for Name: bankaccount; Type: TABLE DATA; Schema: public; Owner: coralplanters
--

INSERT INTO bankaccount
VALUES (1, '3488-225219-15452', 'Gautier et Deschamps', 'VHGWFH2D', 'DK7682543886793171', 9),
       (2, '3746-518564-19203', 'Renaud SAS', 'XSDTFLY261Q', 'EE798867603272340243', 1),
       (3, '6771-8949-7288-9364', 'Pierre et Guillaume', 'RNHEJM6G', 'AL27041892003134moR09vc59i62', 4),
       (4, '3682-118144-8652', 'Vidal et Prévost', 'IEUKLD3HJ7S', 'TN8048863006787193163157', 1),
       (5, '3494-448845-34280', 'Barbier GIE', 'HHSHVJ3R636', 'EE654236755687072321', 6),
       (6, '3468-768754-99358', 'Francois et Schneider', 'VFYVQKFKX0U', 'AE890042662993734421182', 4),
       (7, '6011-6266-2598-4082-8315', 'Garcia SCOP', 'ZPIPFMV333N', 'PS20CDQI18l4ydc48Snn59nHcdy29', 5),
       (8, '3528-9809-4175-9409', 'Prévost et Boyer', 'WIAJYL96890', 'SA2175GS41JO35wYEfjDDn92', 5),
       (9, '6759-2607-1981-4550', 'Mercier et André', 'GDMGEMKM', 'TN3701762850931117025959', 6),
       (10, '3529-4428-7154-0886', 'Dumas et Robert', 'UOGTIT27UOB', 'MU35DYFX9180928358409142058BXW', 2);


--
-- Data for Name: coral; Type: TABLE DATA; Schema: public; Owner: coralplanters
--

INSERT INTO coral
VALUES (1, 'Rerum perferendis nam sit est.', TRUE, 'Prof Evan Nicolas', 'Aperiam modi qui.', 6, NULL),
       (2, 'Magnam a laborum amet quibusdam id. Reprehenderit et et reprehenderit assumenda animi.', FALSE,
        'Gérard Marie',
        'Mollitia distinctio corporis quam alias vel. Suscipit repellendus architecto deleniti consequatur aliquid est. Est voluptatem non et voluptate explicabo qui officia.',
        8, NULL),
       (3, 'Quis mollitia incidunt id. Similique minus nobis eos enim.', FALSE, 'Guérin Noah',
        'Quas est aut nesciunt soluta ratione. Labore numquam quia et placeat unde. Qui est voluptatibus similique ullam aut laudantium nihil.',
        3, NULL),
       (4, 'Nihil sunt rerum omnis error dicta.', TRUE, 'Moreau Benjamin',
        'Molestiae et error odit sed hic consequatur.', 7, 1),
       (5, 'Excepturi consectetur odit eos. Et sint blanditiis iure facilis. Adipisci voluptas itaque laudantium.',
        FALSE, 'Prof Clara Le gall', 'Non eum sed beatae.', 4, NULL),
       (6, 'Nostrum sit est et.', TRUE, 'Mlle Anaïs Breton',
        'Placeat reprehenderit reiciendis voluptatibus quis molestias laborum vel.', 2, 2),
       (7, 'Dolor aut et rerum enim ullam.', FALSE, 'Dr Benjamin Fleury', 'Eos minima tempora dolorum.', 1, NULL),
       (8, 'Natus repudiandae nam et nulla sit. Tenetur quidem eum eligendi.', FALSE, 'Dr Adam Vidal',
        'Eum non aut ut.', 8, NULL),
       (9, 'Ullam aliquam sint reiciendis. Voluptate voluptas sint reprehenderit. Voluptatem ipsa iure sunt rerum.',
        TRUE, 'Dupuis Mathis',
        'Omnis ut dolorem et dolore corrupti fugit. Magni eius ipsa qui. Debitis facilis tempore nihil quos sunt.', 4,
        6),
       (10, 'Voluptatum blanditiis sit sequi quod.', FALSE, 'Alexandre Rodriguez',
        'Tempore exercitationem voluptate dolore consequatur qui. Beatae eveniet nisi unde atque optio sit. Eveniet quia iusto dignissimos provident porro.',
        5, NULL),
       (11, 'Modi at architecto impedit.', TRUE, 'Mathilde Laurent', 'Et neque in facere.', 5, 4),
       (12, 'Explicabo repellat odit pariatur laudantium velit et ab. Est praesentium omnis architecto soluta est rem.',
        FALSE, 'Mme Lina Baron',
        'Non dolorum omnis distinctio voluptatum exercitationem. Consequatur dolores consequatur aliquam.', 7, NULL),
       (13,
        'Officia ratione modi omnis iure doloremque. Eum omnis nihil facilis consequatur sunt. Officia expedita maiores quae veritatis.',
        TRUE, 'Mlle Romain Aubert', 'Voluptate cum ducimus voluptatem eos qui alias fugit.', 7, 10),
       (14, 'Sint voluptas sit provident ad. Veniam voluptatibus incidunt ut hic ut nobis.', FALSE, 'Huet Gabriel',
        'Ut molestiae laborum voluptatum porro facilis. Natus rerum reprehenderit vel voluptatum amet autem non.', 3,
        NULL),
       (15, 'Dolorem cupiditate et autem ut sed impedit.', FALSE, 'Renard Alice',
        'Sint a officia ea rerum rerum ab corporis. Eum quis laudantium aliquid et maiores et rem.', 8, NULL),
       (16, 'Vel ducimus quam. Aut quia tenetur vitae.', FALSE, 'Mme Lina Leroy',
        'Nemo perferendis necessitatibus. Distinctio aut error et labore. Blanditiis hic non voluptas eligendi non temporibus.',
        2, NULL),
       (17, 'Et earum harum quis non praesentium et neque. Veniam quas occaecati suscipit.', TRUE, 'Dvnis Lena',
        'Et velit impedit sunt tenetur sit quae nulla.', 3, 9),
       (18, 'Quisquam in accusamus nam. At totam rerum sunt in natus.', TRUE, 'Clément Girard',
        'Natus deserunt ea eligendi debitis consequuntur natus fugiat. Ipsa consequatur tempora ut occaecati maxime.',
        6, 11),
       (19, 'Dolore voluptatum minima dolor velit.', FALSE, 'Mme Rayan Brunet',
        'Optio itaque velit magni et. Quaerat qui non.', 4, NULL),
       (20,
        'Doloribus quasi voluptatem ut rerum deleniti perspiciatis. Rem quis culpa suscipit tempore perferendis. Est amet quasi perspiciatis ullam neque.',
        FALSE, 'Prof Pauline Giraud', 'Voluptate ad labore esse est.', 5, NULL),
       (21, 'Deserunt expedita ullam perspiciatis quibusdam sequi et. Suscipit totam aut sed quam.', FALSE,
        'Mlle Eva Moulin', 'Et ut illo quia et nihil asperiores quam.', 2, NULL),
       (22, 'Reprehenderit ullam consequuntur molestias a. Autem repudiandae doloribus voluptas est qui animi.', TRUE,
        'Vidal Jules', 'Ea in tenetur accusantium autem eaque. Eveniet qui nostrum expedita aliquid.', 8, 19),
       (23, 'Et nemo vero natus earum ipsum. Tempora molestias tenetur ducimus. Voluptatibus quas sed.', FALSE,
        'Prof Julie Laurent', 'Explicabo eligendi eaque quasi perspiciatis incidunt.', 6, NULL),
       (24,
        'Perferendis omnis sint maxime aut doloribus. Enim et ut minus numquam deleniti est aliquam. Saepe qui recusandae necessitatibus repellat.',
        FALSE, 'M Pauline Jean',
        'Sit facilis laborum mollitia eum dolor possimus. Voluptatum unde est quis illo molestiae cupiditate culpa. Voluptates est veniam dignissimos nihil voluptates aut voluptas.',
        4, NULL),
       (25, 'Aut voluptatem quos. Eaque minima inventore aliquid. Aspernatur sed ut voluptatem labore quidem.', FALSE,
        'Dr Maxence Dupont', 'Tenetur modi nostrum sed officiis.', 9, NULL),
       (26, 'Ut unde fuga itaque. Repellendus consequatur id.', TRUE, 'M Louis Paris',
        'In vel dignissimos nostrum dicta autem optio. Dicta odio quibusdam incidunt praesentium at aperiam. Quia sapiente omnis non provident ab.',
        5, 15),
       (27,
        'Omnis sit autem voluptatem numquam maiores vero eligendi. Rerum voluptas qui voluptatem repudiandae reprehenderit sed.',
        TRUE, 'Mlle Romain Renard', 'Id officiis quis voluptatem et ut perspiciatis distinctio.', 8, 5),
       (28, 'Doloremque deserunt nesciunt.', TRUE, 'Breton Océane',
        'Dolorem nihil doloribus minima molestiae et sit necessitatibus. Qui aliquam est ullam et et ut.', 2, 19),
       (29, 'Possimus vero sit atque rerum repellendus.', TRUE, 'Robert Antoine',
        'Est voluptate delectus corrupti sint. Vel officia atque natus officia et culpa.', 7, 22),
       (30,
        'Et voluptatibus recusandae assumenda laborum dolorem eos natus. Et assumenda explicabo. Minus architecto neque expedita sed illum.',
        TRUE, 'Royer Eva',
        'In perferendis distinctio sed ipsum voluptatem dolores voluptatem. Dolor ut corrupti dolorum molestias laborum nesciunt.',
        5, 16),
       (31,
        'Mollitia dolorem quia autem sed veniam excepturi enim. Impedit qui id quae nobis quia. Non sit temporibus amet amet at omnis.',
        FALSE, 'Dr Nathan Arnaud',
        'Quia occaecati numquam repellendus sunt molestias. Et quo harum cum ea molestias exercitationem rerum.', 8,
        NULL),
       (32,
        'Nihil corporis id voluptates laudantium aperiam qui. Et quis dolorum. Non nobis voluptatem dolores est est aut.',
        FALSE, 'Dvnis Mathis', 'Temporibus qui qui voluptatem reprehenderit possimus consequuntur.', 2, NULL),
       (33,
        'Maxime illo voluptatibus molestiae laboriosam perspiciatis magnam mollitia. Voluptatem consectetur et sequi.',
        FALSE, 'Morel Elisa', 'Ut eius laudantium. Ut quaerat autem est illum officiis quis.', 4, NULL),
       (34,
        'Nemo nulla qui velit illo voluptatem asperiores et. Error illo quisquam expedita reiciendis. Laborum tenetur omnis vel sunt et occaecati.',
        FALSE, 'Nathan Ménard', 'Eum quisquam minima necessitatibus.', 1, NULL),
       (35, 'Iure unde temporibus pariatur nulla repudiandae culpa. Et assumenda est ut inventore voluptas aut at.',
        FALSE, 'Dr Hugo Fleury', 'Corrupti rem modi ab et repudiandae placeat.', 6, NULL),
       (36, 'Magni est labore sapiente ipsa.', FALSE, 'Dr Lina Mathieu',
        'Cum rerum debitis rerum itaque dolores impedit. Illum praesentium incidunt voluptatum ut ipsa. Velit sint corrupti laboriosam est.',
        8, NULL),
       (37, 'Provident tempore neque.', FALSE, 'Dr Lucas Bernard', 'Corporis in quia dolores eos.', 9, NULL),
       (38, 'Et quia ea. Dolorem omnis dolores at quidem vero nostrum dolores.', FALSE, 'Gauthier Alicia',
        'Dicta eligendi sint. Qui non ipsum sunt libero qui ut. Quas cupiditate natus harum.', 9, NULL),
       (39, 'Commodi quaerat sequi quo quae sunt qui numquam. Molestiae nemo quibusdam maxime aut mollitia aut odio.',
        TRUE, 'Alicia Méunier',
        'Inventore aspernatur id quaerat. Iure aspernatur dolor omnis et fuga laudantium quis. Ipsam doloremque eveniet id earum.',
        4, 28),
       (40,
        'Voluptatibus nihil voluptatem eum. Officiis adipisci rerum animi dolorum esse quis. Consequatur officia aspernatur nostrum.',
        FALSE, 'Mélissa Moreau',
        'Consequuntur aut dolorum ipsa earum dolore ex optio. Provident quis vel. Quia sequi repudiandae fugit sapiente doloribus est qui.',
        7, NULL),
       (41, 'Rem iure perferendis quas illum. Ipsum eligendi non.', FALSE, 'Dupuy Louis', 'Quia qui nisi.', 5, NULL),
       (42, 'Voluptatem ea repudiandae natus corrupti. Et in reprehenderit error quia nam aspernatur.', FALSE,
        'Olivier Anaïs', 'Dolorum atque quo qui.', 7, NULL),
       (43, 'Quos cum cupiditate. Sint officiis dolores.', TRUE, 'Martinez Tom',
        'Sint commodi eligendi. Natus delectus voluptate. Itaque placeat voluptates voluptas.', 8, 27),
       (44, 'Alias optio magnam ipsam cupiditate.', FALSE, 'Célia Gautier', 'Qui et eaque enim fugit nihil a.', 2,
        NULL),
       (45, 'Fuga voluptas tempora facere tenetur porro. Illo aspernatur laboriosam ut ullam at aliquid.', TRUE,
        'Petit Adrien', 'Nemo suscipit omnis nobis aut.', 3, 26),
       (46,
        'Ea exercitationem voluptatibus aperiam aut et eum. Soluta aperiam mollitia totam similique aut. Sint dolore ea et et eveniet.',
        TRUE, 'Arnaud Mathilde',
        'Modi magnam et amet. Nihil est sed fugit enim itaque eos et. Omnis et enim omnis recusandae rerum labore consequuntur.',
        5, 33),
       (47, 'Vitae est sit.', TRUE, 'Justine Vasseur',
        'Sunt et suscipit aut consequatur. Deleniti qui ducimus hic voluptas nulla. Est recusandae et.', 6, 6),
       (48,
        'Consectetur sequi similique et. Voluptatibus aliquid unde quasi enim culpa. Nihil quam est accusamus laborum sit aut ab.',
        FALSE, 'Rolland Lola', 'Voluptatum nesciunt laudantium ducimus placeat impedit id.', 9, NULL),
       (49, 'Ratione voluptatem ad minima et facere.', TRUE, 'Garnier Romane',
        'Labore porro excepturi magni accusantium totam rerum nihil. Vitae non harum voluptatem quia. Consequatur nemo cum.',
        3, 8),
       (50, 'Beatae nam asperiores exercitationem facilis possimus quia et. Dolore hic ut possimus nihil.', TRUE,
        'Mme Célia Carpentier',
        'Repellendus quaerat voluptatem molestiae laboriosam. Consequuntur voluptas culpa porro in sit illum.', 5, 19);


--
-- Data for Name: group_user; Type: TABLE DATA; Schema: public; Owner: coralplanters
--

INSERT INTO group_user
VALUES (1, 9),
       (2, 7),
       (2, 6),
       (2, 2),
       (2, 2),
       (2, 4),
       (2, 9),
       (2, 4),
       (3, 2),
       (3, 6),
       (3, 1),
       (3, 5),
       (3, 3),
       (4, 2),
       (4, 2),
       (4, 2),
       (4, 7),
       (4, 9),
       (4, 3),
       (5, 1),
       (5, 2),
       (5, 2),
       (5, 3),
       (5, 8),
       (5, 2),
       (5, 2);


--
-- Data for Name: resource; Type: TABLE DATA; Schema: public; Owner: coralplanters
--

INSERT INTO resource
VALUES (1, '1986-08-08 22:12:39.076', 'www.victor-le roux.bzh', 1, 2),
       (2, '2002-11-27 15:20:23.023', 'www.hugo-vasseur.com', 2, 2),
       (3, '1986-12-11 12:15:16.476', 'www.maxence-roussel.name', 3, 2),
       (4, '1981-03-03 16:54:51.805', 'www.gabriel-berger.paris', 4, 2),
       (5, '1977-11-13 09:26:08.439', 'www.pierre-le roux.com', 5, 2),
       (6, '1970-02-22 12:38:13.677', 'www.xn--la-xn--andr-epa-bnb.fr', 6, 2),
       (7, '1984-01-07 06:22:23.859', 'www.jeanne-xn--prvost-cva.com', 7, 2),
       (8, '1976-07-14 06:56:36.378', 'www.laura-garcia.eu', 8, 1),
       (9, '1964-10-25 14:40:12.698', 'www.paul-guillaume.info', 9, 2),
       (10, '1975-03-09 07:34:50.617', 'www.rayan-nguyen.immo', 10, 3),
       (11, '1959-06-12 03:21:46.074', 'www.thomas-francois.alsace', 11, 2),
       (12, '1988-11-17 22:57:51.761', 'www.julien-blanchard.name', 12, 2),
       (13, '1978-08-14 14:16:17.421', 'www.alicia-xn--munier-bva.fr', 13, 2),
       (14, '1972-01-12 05:35:04.409', 'www.juliette-deschamps.bzh', 14, 1),
       (15, '1988-01-21 19:16:08.627', 'www.noa-guyot.info', 15, 2),
       (16, '1976-01-25 20:18:13.418', 'www.marie-hubert.eu', 16, 2),
       (17, '1982-09-11 16:34:53.609', 'www.carla-lambert.eu', 17, 2),
       (18, '1974-02-17 21:49:00.518', 'www.xn--clia-bourgeois-bkb.info', 18, 1),
       (19, '1976-07-17 14:39:35.752', 'www.xn--clia-rey-b1a.net', 19, 1),
       (20, '1995-07-09 11:48:50.921', 'www.baptiste-dubois.org', 20, 2),
       (21, '2001-10-06 15:47:37.87', 'www.elisa-jacquet.com', 21, 1),
       (22, '2004-05-06 03:30:32.18', 'www.xn--ocane-lemaire-chb.info', 22, 2),
       (23, '1991-10-09 04:30:56.893', 'www.lina-chevalier.net', 23, 2),
       (24, '1991-05-30 23:58:34.999', 'www.tom-vidal.paris', 24, 2),
       (25, '1986-07-08 22:40:02.01', 'www.julien-xn--mnard-bsa.com', 25, 2),
       (26, '1964-02-29 17:19:13.775', 'www.xn--clmence-simon-chb.net', 26, 2),
       (27, '1966-06-17 10:08:47.335', 'www.lilou-moreau.org', 27, 1),
       (28, '1973-07-04 18:47:50.177', 'www.noah-perrot.name', 28, 2),
       (29, '1974-12-19 18:50:50.203', 'www.xn--clmence-mathieu-cnb.immo', 29, 2),
       (30, '1975-03-02 16:55:28.646', 'www.julien-boyer.fr', 30, 1),
       (31, '2004-02-22 00:34:35.291', 'www.manon-rolland.com', 31, 2),
       (32, '1970-03-01 14:01:39.744', 'www.quentin-leclercq.com', 32, 3),
       (33, '2002-02-24 15:15:17.509', 'www.ambre-paul.immo', 33, 1),
       (34, '2003-12-09 16:01:22.571', 'www.axel-charpentier.info', 34, 2),
       (35, '1999-03-19 00:35:43.305', 'www.maeva-roger.org', 35, 1),
       (36, '2004-01-09 15:38:24.152', 'www.xn--matto-nguyen-eeb.eu', 36, 2),
       (37, '1979-03-10 00:19:49.671', 'www.mathilde-da silva.alsace', 37, 3),
       (38, '2005-03-05 22:22:11.867', 'www.xn--matho-leroy-ebb.paris', 38, 2),
       (39, '1978-01-04 23:47:09.579', 'www.victor-dubois.immo', 39, 3),
       (40, '1992-05-30 20:27:56.132', 'www.axel-xn--rivire-6ua.info', 40, 2),
       (41, '1999-11-01 19:07:41.147', 'www.xn--clmence-roy-cbb.bzh', 41, 1),
       (42, '1992-07-21 04:10:40.123', 'www.lucas-richard.immo', 42, 1),
       (43, '1996-06-11 22:42:21.304', 'www.ambre-roger.com', 43, 2),
       (44, '1969-10-24 14:59:29.937', 'www.lina-richard.net', 44, 2),
       (45, '1976-01-23 19:29:35.932', 'www.camille-carpentier.fr', 45, 2),
       (46, '2001-02-14 23:00:55.492', 'www.xn--malle-garnier-4hb.name', 46, 2),
       (47, '1995-03-30 14:17:25.757', 'www.maxence-baron.bzh', 47, 2),
       (48, '1990-11-30 08:14:42.473', 'www.elisa-rodriguez.fr', 48, 2),
       (49, '1963-12-28 18:13:04.476', 'www.camille-marchand.paris', 49, 2),
       (50, '1994-05-27 05:15:49.824', 'www.tom-roger.info', 50, 2);


--
-- Data for Name: resourcetype; Type: TABLE DATA; Schema: public; Owner: coralplanters
--

INSERT INTO resourcetype
VALUES (1, 'Image'),
       (2, 'Video'),
       (3, 'Text'),
       (4, 'Audio');


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: coralplanters
--

INSERT INTO role
VALUES (1, 'USER'),
       (2, 'ADMIN'),
       (3, 'GUEST');


--
-- Data for Name: sponsorship; Type: TABLE DATA; Schema: public; Owner: coralplanters
--

INSERT INTO sponsorship
VALUES (1, 3101, '1958-06-21 12:24:26.231', 9, 6),
       (2, 9497, '1964-02-16 04:49:15.752', 29, 3),
       (3, 3502, '1994-02-04 02:31:06.596', 39, 5),
       (4, 6636, '1965-10-18 14:48:47.794', 49, 5);


--
-- Data for Name: structure; Type: TABLE DATA; Schema: public; Owner: coralplanters
--

INSERT INTO structure
VALUES (1, 'Practical Steel Gloves', 3),
       (2, 'Lightweight Silk Bench', 6),
       (3, 'Enormous Plastic Knife', 3),
       (4, 'Rustic Iron Bench', 3),
       (5, 'Lightweight Rubber Bench', 7);


--
-- Data for Name: user_address; Type: TABLE DATA; Schema: public; Owner: coralplanters
--

INSERT INTO user_address
VALUES (1, 6),
       (1, 4),
       (2, 5),
       (2, 1),
       (2, 4),
       (3, 3),
       (3, 2),
       (4, 7),
       (4, 2),
       (5, 5),
       (5, 1),
       (6, 9),
       (6, 8),
       (7, 7),
       (8, 3),
       (8, 8),
       (9, 4),
       (9, 3),
       (9, 5),
       (10, 4),
       (10, 3);


--
-- Name: 'group'_idgroup_seq; Type: SEQUENCE SET; Schema: public; Owner: coralplanters
--

SELECT pg_catalog.SETVAL('"''group''_idgroup_seq"', 5, TRUE);


--
-- Name: 'user'_iduser_seq; Type: SEQUENCE SET; Schema: public; Owner: coralplanters
--

SELECT pg_catalog.SETVAL('"''user''_iduser_seq"', 10, TRUE);


--
-- Name: address_idaddress_seq; Type: SEQUENCE SET; Schema: public; Owner: coralplanters
--

SELECT pg_catalog.SETVAL('address_idaddress_seq', 10, TRUE);


--
-- Name: bankaccount_idbankaccount_seq; Type: SEQUENCE SET; Schema: public; Owner: coralplanters
--

SELECT pg_catalog.SETVAL('bankaccount_idbankaccount_seq', 10, TRUE);


--
-- Name: coral_idcoral_seq; Type: SEQUENCE SET; Schema: public; Owner: coralplanters
--

SELECT pg_catalog.SETVAL('coral_idcoral_seq', 50, TRUE);


--
-- Name: resource_idresource_seq; Type: SEQUENCE SET; Schema: public; Owner: coralplanters
--

SELECT pg_catalog.SETVAL('resource_idresource_seq', 50, TRUE);


--
-- Name: resourcetype_idresourcetype_seq; Type: SEQUENCE SET; Schema: public; Owner: coralplanters
--

SELECT pg_catalog.SETVAL('resourcetype_idresourcetype_seq', 4, TRUE);


--
-- Name: role_idrole_seq; Type: SEQUENCE SET; Schema: public; Owner: coralplanters
--

SELECT pg_catalog.SETVAL('role_idrole_seq', 3, TRUE);


--
-- Name: sponsorship_idsponsorship_seq; Type: SEQUENCE SET; Schema: public; Owner: coralplanters
--

SELECT pg_catalog.SETVAL('sponsorship_idsponsorship_seq', 4, TRUE);


--
-- Name: structure_idstructure_seq; Type: SEQUENCE SET; Schema: public; Owner: coralplanters
--

SELECT pg_catalog.SETVAL('structure_idstructure_seq', 5, TRUE);


--
-- Name: 'group' 'group'_pkey; Type: CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY "'group'"
    ADD CONSTRAINT "'group'_pkey"
        PRIMARY KEY (idgroup);


--
-- Name: 'user' 'user'_pkey; Type: CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY "'user'"
    ADD CONSTRAINT "'user'_pkey"
        PRIMARY KEY (iduser);


--
-- Name: address address_pkey; Type: CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey
        PRIMARY KEY (idaddress);


--
-- Name: bankaccount bankaccount_pkey; Type: CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY bankaccount
    ADD CONSTRAINT bankaccount_pkey
        PRIMARY KEY (idbankaccount);


--
-- Name: coral coral_pkey; Type: CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY coral
    ADD CONSTRAINT coral_pkey
        PRIMARY KEY (idcoral);


--
-- Name: resource resource_pkey; Type: CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY resource
    ADD CONSTRAINT resource_pkey
        PRIMARY KEY (idresource);


--
-- Name: resourcetype resourcetype_pkey; Type: CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY resourcetype
    ADD CONSTRAINT resourcetype_pkey
        PRIMARY KEY (idresourcetype);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey
        PRIMARY KEY (idrole);


--
-- Name: sponsorship sponsorship_pkey; Type: CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY sponsorship
    ADD CONSTRAINT sponsorship_pkey
        PRIMARY KEY (idsponsorship);


--
-- Name: structure structure_pkey; Type: CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY structure
    ADD CONSTRAINT structure_pkey
        PRIMARY KEY (idstructure);


--
-- Name: bankaccount fk_bankaccount_user; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY bankaccount
    ADD CONSTRAINT fk_bankaccount_user
        FOREIGN KEY (user_id)
            REFERENCES "'user'" (iduser);


--
-- Name: coral fk_coral_address; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY coral
    ADD CONSTRAINT fk_coral_address
        FOREIGN KEY (address_id)
            REFERENCES address (idaddress);


--
-- Name: coral fk_coral_parent; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY coral
    ADD CONSTRAINT fk_coral_parent
        FOREIGN KEY (coral_parent_id)
            REFERENCES coral (idcoral);


--
-- Name: 'group' fk_group_admin; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY "'group'"
    ADD CONSTRAINT fk_group_admin
        FOREIGN KEY (admin_id)
            REFERENCES "'user'" (iduser);


--
-- Name: 'group' fk_group_creator; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY "'group'"
    ADD CONSTRAINT fk_group_creator
        FOREIGN KEY (creator_id)
            REFERENCES "'user'" (iduser);


--
-- Name: 'group' fk_group_structure; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY "'group'"
    ADD CONSTRAINT fk_group_structure
        FOREIGN KEY (structure_id)
            REFERENCES structure (idstructure);


--
-- Name: group_user fk_group_user_group; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY group_user
    ADD CONSTRAINT fk_group_user_group
        FOREIGN KEY (group_id)
            REFERENCES "'group'" (idgroup);


--
-- Name: group_user fk_group_user_user; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY group_user
    ADD CONSTRAINT fk_group_user_user
        FOREIGN KEY (user_id)
            REFERENCES "'user'" (iduser);


--
-- Name: resource fk_resource_coral; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY resource
    ADD CONSTRAINT fk_resource_coral
        FOREIGN KEY (coral_id)
            REFERENCES coral (idcoral);


--
-- Name: resource fk_resource_resourcetype; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY resource
    ADD CONSTRAINT fk_resource_resourcetype
        FOREIGN KEY (resourcetype_id)
            REFERENCES resourcetype (idresourcetype);


--
-- Name: sponsorship fk_sponsorship_coral; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY sponsorship
    ADD CONSTRAINT fk_sponsorship_coral
        FOREIGN KEY (coral_id)
            REFERENCES coral (idcoral);


--
-- Name: sponsorship fk_sponsorship_user; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY sponsorship
    ADD CONSTRAINT fk_sponsorship_user
        FOREIGN KEY (sponsor_id)
            REFERENCES "'user'" (iduser);


--
-- Name: structure fk_structure_address; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY structure
    ADD CONSTRAINT fk_structure_address
        FOREIGN KEY (address_id)
            REFERENCES address (idaddress);


--
-- Name: user_address fk_user_address_address; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY user_address
    ADD CONSTRAINT fk_user_address_address
        FOREIGN KEY (address_id)
            REFERENCES address (idaddress);


--
-- Name: user_address fk_user_address_user; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY user_address
    ADD CONSTRAINT fk_user_address_user
        FOREIGN KEY (user_id)
            REFERENCES "'user'" (iduser);


--
-- Name: 'user' fk_user_role; Type: FK CONSTRAINT; Schema: public; Owner: coralplanters
--

ALTER TABLE ONLY "'user'"
    ADD CONSTRAINT fk_user_role
        FOREIGN KEY (role_id)
            REFERENCES role (idrole);


--
-- PostgreSQL database dump complete
--
