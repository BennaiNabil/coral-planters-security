# Coral Planters

<!-- TOC -->
* [Coral Planters](#coral-planters)
  * [Introduction](#introduction)
  * [Prérequis](#prérequis)
  * [Installation](#installation)
  * [Exécution de l'API Spring Boot](#exécution-de-lapi-spring-boot)
    * [Etape 0 : Prérequis](#etape-0--prérequis)
    * [Étape 1 : Configuration de la base de données](#étape-1--configuration-de-la-base-de-données)
    * [Étape 2 : Lancement de l'application Spring Boot](#étape-2--lancement-de-lapplication-spring-boot)
    * [Étape 3 : Vérification de l'exécution de l'application](#étape-3--vérification-de-lexécution-de-lapplication)
  * [Réinitialisation de la base de données](#réinitialisation-de-la-base-de-données)
  * [Tests unitaires](#tests-unitaires)
    * [Etape 1 : Identifiez chaque fonctionnalité de votre code](#etape-1--identifiez-chaque-fonctionnalité-de-votre-code)
    * [Etape 2 : Écrivez des tests vides pour chaque fonctionnalité](#etape-2--écrivez-des-tests-vides-pour-chaque-fonctionnalité)
    * [Etape 3 : Utilisez des outils de test](#etape-3--utilisez-des-outils-de-test)
    * [Etape 4 : Ecrire les assertions](#etape-4--ecrire-les-assertions)
    * [Etape 5 : Exécutez les tests](#etape-5--exécutez-les-tests)
<!-- TOC -->

## Introduction

Coral Planters est une application web permettant de planter des coraux. Le backend est développé en utilisant le
framework Spring Boot et le frontend est développé en utilisant le framework Angular. La base de données est contenue
dans un docker-compose et contient un service qui exécute Postgres.

## Prérequis

- Java 17+
- Docker
- Docker-compose
- Angular CLI
- [Postman](https://www.postman.com) (facultatif)

## Installation

Le script shell [create_db.sh](./create_db.sh) est un script pour gérer la base de données du projet Coral Planters. Ce
script
permet aux développeurs de gérer la base de données Docker sans avoir à utiliser des commandes Docker complexes. Il a
plusieurs fonctionnalités utiles pour les développeurs :

- Il vérifie si Docker est en cours d'exécution avant de commencer.
- Il permet aux développeurs de démarrer et d'arrêter facilement le conteneur Docker pour la base de données.
- Il permet aux développeurs de sauvegarder la base de données en spécifiant un nom de fichier ou en utilisant un nom de
  fichier par défaut.
- Il donne la possibilité aux utilisateurs de quitter le script à tout moment en entrant 'exit' ou 'q'.
- Le script contient un menu de sélection pour l'utilisateur, avec des options différentes en fonction de l'État actuel
  du conteneur Docker :
    - Si le conteneur est arrêté, les options sont de le démarrer ou de quitter le script.
    - Si le conteneur est en cours d'exécution, les options sont de l'arrêter, de sauvegarder la base de données ou de
      quitter le script.
    - Le script est facile à utiliser et offre une interface conviviale pour les développeurs du projet Coral Planters.

Pour utiliser le script, exécutez les commandes suivantes :

```bash
# Rend le script exécutable
chmod +x create_db.sh

# Exécute le script 
./create_db.sh 
```

## Exécution de l'API Spring Boot

### Etape 0 : Prérequis

- Java 17 ou une version plus récente doit être installée sur votre ordinateur.
- Assurez-vous que la base de données PostgreSQL est en cours d'exécution en exécutant le script shell mentionné dans la
  section précédente.

### Étape 1 : Configuration de la base de données

Avant de lancer le backend Spring, assurez-vous que la configuration de la base de données dans le fichier
[application.yml](./src/main/resources/application.yml) est correcte. Le fichier de configuration est de la forme
suivante :

```yaml
spring:
    datasource:
        url: [ URL de la base de données ]
        username: [ Nom d'utilisateur de la base de données ]
        password: [ Mot de passe de la base de données ]
        driver-class-name: org.postgresql.Driver
    jpa:
        show-sql: [ true|false ]
```

### Étape 2 : Lancement de l'application Spring Boot

- Ouvrez un terminal et allez dans le dossier racine du projet backend.
- Exécutez la commande suivante pour compiler l'application. Cette commande va télécharger les dépendances,
  compiler les sources et exécuter les tests.

```bash
./mvnw clean install
```

- Ensuite, exécutez la commande suivante pour lancer l'application Spring Boot. Cela va lancer l'application sur le port
  par défaut 8080.

```bash
./mvnw spring-boot:run
  ```

### Étape 3 : Vérification de l'exécution de l'application

- Pour vérifier que l'application est en cours d'exécution, ouvrez un navigateur web et accédez à
  l'URL http://localhost:8080/actuator/health.
- Si l'application est en cours d'exécution, vous devriez voir le message
  suivant dans la réponse JSON renvoyée.

```json
{
    "status": "UP"
}
```

- Pour tester les points de terminaison (endpoints) de l'API, nous avons créé une collection de requêtes Postman que
  vous pouvez trouver dans le
  fichier [coralplanters_collection.json](./documents/api/postman/coralplanters_collection.json) dans le
  dossier ```documents/api/postman```. Cette collection de requêtes Postman contient des requêtes pour tous les
  endpoints de l'API. Il est recommandé de mettre à jour cette collection à chaque fois qu'un nouvel endpoint est ajouté
  à l'API.
  ![](./documents/images/exemple_postman.png)
- Si, pour une raison quelconque, vous ne pouvez pas utiliser Postman, il y a des fichiers avec l'extension http dans le
  dossier documents/api/http_requests. Ces fichiers contiennent des requêtes HTTP basiques pour tester les endpoints de
  l'API. Il est également recommandé de créer de nouveaux fichiers avec l'extension http à chaque fois qu'un nouvel
  endpoint est ajouté à l'API. La convention est de créer un fichier par contrôleur REST.
  ![](./documents/images/exemple_client_http.png)

## Réinitialisation de la base de données

Dans le projet Coral Planters, nous avons créé un custom goal pour réinitialiser la base de données en redémarrant le
conteneur Docker. Ce custom goal est exécuté à l'aide du plugin exec-maven-plugin. Le code pour ce custom goal est
présenté dans l'énoncé de la question.

Le custom goal "reset-database" utilise le plugin exec-maven-plugin pour exécuter une commande shell qui redémarre le
conteneur Docker contenant la base de données PostgreSQL. La commande exécute deux actions : elle arrête le conteneur
Docker et le redémarre en utilisant le fichier de configuration docker-compose.yml. En redémarrant le conteneur Docker,
cela réinitialise la base de données en supprimant son contenu et en réinitialisant les données à leur état initial.

En utilisant ce custom goal, les développeurs peuvent facilement réinitialiser la base de données PostgreSQL en
exécutant la commande suivante à partir de la ligne de commande. Cela simplifie la gestion de la base de données pour
les développeurs, en leur permettant de réinitialiser rapidement la base de données en cas de besoin.

```bash
./mvnw exec:exec@reset-database
```

## Tests unitaires

Les tests unitaires sont une méthode de test qui vous permet de vérifier le comportement de chaque composant de votre
code de manière isolée. Ils peuvent vous aider à identifier les erreurs plus tôt dans le cycle de développement et à
garantir que votre application fonctionne correctement.

Voici un guide étape par étape pour écrire des tests unitaires <i>"à la Coral Planters"</i>:

### Etape 1 : Identifiez chaque fonctionnalité de votre code

```java

@RestController
@RequestMapping("/api/role")
public class RoleController extends ControllerCRUD<Role, RoleRepository> {
// ...
}
```

Dans cet exemple, nous avons identifié RoleController comme contenant des fonctionnalités de notre application.

### Etape 2 : Écrivez des tests vides pour chaque fonctionnalité

```java
package org.simplon.coralplanters.controllers;

public class RoleControllerTest {

    @Test
    @DisplayName("Find all roles")
    void testFindAllShouldReturnAllRoles() {
        // TODO: Test pour testFindAllShouldReturnAllRoles()
    }

    @Test
    @DisplayName("Find role by ID")
    void testFindByIdShouldReturnRoleWhenFound() {
        // TODO: Test pour testFindByIdShouldReturnRoleWhenFound()
    }

    // ...

}

```

Nous avons écrit des tests unitaires pour RoleController en utilisant JUnit et Mockito. Chaque test couvre une
fonctionnalité différente de RoleController.

### Etape 3 : Utilisez des outils de test

```java
public class RoleControllerTest {

    @Mock
    private RoleService roleService;

    @InjectMocks
    private RoleController roleController;
    private Role role1;
    private Role role2;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        role1 = new Role();
        role1.setId(1L);
        role1.setRole("ROLE_ADMIN");

        role2 = new Role();
        role2.setId(2L);
        role2.setRole("ROLE_USER");
    }
    
    // ...
}
```

Nous utilisons Mockito pour créer un objet fictif (repoMock) qui simule les dépendances de RoleController. Nous
utilisons également des annotations JUnit telles que @BeforeEach pour configurer l'environnement de test.

### Etape 4 : Ecrire les assertions

```java
public class RoleControllerTest {

    @Mock
    private RoleRepository repoMock;

    private RoleController controller;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        controller = new RoleController(repoMock);
    }
    
    @Test
    void testFindAllShouldReturnAllRoles() {
        List<Role> roles = Arrays.asList(role1, role2);
        when(roleService.findAll()).thenReturn(roles);

        List<Role> result = roleController.findAll();

        assertEquals(2, result.size());
        assertEquals("ROLE_ADMIN", result.get(0).getRole());
        assertEquals("ROLE_USER", result.get(1).getRole());
    }

    // ...
}
```

Dans chaque test, nous utilisons des assertions JUnit telles que assertEquals pour vérifier que le résultat est correct.
Si l'assertion échoue, le test échoue également.

### Etape 5 : Exécutez les tests

Lorsque vous avez écrit vos tests unitaires, il est important de les exécuter pour s'assurer qu'ils sont tous
fonctionnels. Les tests unitaires peuvent être exécutés à partir de votre IDE ou via un outil de construction comme
Maven.

Dans Eclipse, vous pouvez exécuter tous vos tests unitaires en cliquant avec le bouton droit de la souris sur votre
dossier de test et en sélectionnant <b>"Run as JUnit Test"</b> dans le menu déroulant. (De mémoire, à vérifier... De
toute façon IntelliJ est mieux 😜)

Dans IntelliJ IDEA, vous pouvez exécuter tous vos tests unitaires en cliquant avec le bouton droit de la souris sur
votre dossier de test et en sélectionnant <b>"Run 'All Tests'"</b> dans le menu déroulant.

En utilisant Maven, vous pouvez exécuter tous vos tests unitaires en exécutant la commande suivante à partir de votre
terminal :

```bash
./mvnw test
```

Pour faciliter le partage de résultats de tests, et pour soi-même lire ces
résultats plus facilement, on peut utiliser le plugin surefire-report de Maven :

```xml

<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-surefire-report-plugin</artifactId>
    <version>3.0.0-M5</version>
</plugin>
```

Ensuite, on peut exécuter les tests unitaires avec la commande suivante :

```bash
chmod +x generate_test_report.sh

./generate_test_report.sh

```

Le script [generate_test_report.sh](./generate_test_report.sh) génère un rapport détaillé des résultats des tests de
votre projet. Il est facile à utiliser : exécutez-le pour lancer tous les tests de votre projet et visualiser les
résultats dans un rapport facile à comprendre. Vous pouvez rapidement voir quels tests ont réussi, échoué ou été
ignorés, ce qui peut vous aider à résoudre les problèmes de votre application plus rapidement. Le rapport peut également
servir de documentation pour les tests de votre projet et faciliter la collaboration avec d'autres développeurs. Ce
script est compatible avec Windows, macOS et Linux pour une accessibilité aisée.

Les tests unitaires sont essentiels pour garantir la qualité de votre code et faciliter la maintenance de votre
application. Il est important de tester chaque endpoint et processeur, et de maintenir une couverture de code élevée
pour détecter les erreurs tôt dans le processus de développement. Les bibliothèques de tests telles que JUnit et Mockito
permettent de simuler des conditions de test et de vérifier que le code fonctionne correctement. En résumé, les tests
unitaires sont un élément clé pour la réussite de votre projet.




