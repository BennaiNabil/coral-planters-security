#!/bin/bash

COMPOSE_FILE="./documents/database/docker-compose.yml"

# Vérifier si Docker est en cours d'exécution
if ! docker info >/dev/null 2>&1; then
    echo "Erreur: Docker n'est pas en cours d'exécution. Veuillez démarrer Docker et réessayer."
    exit 1
fi

# Fonction pour vérifier si le conteneur est en cours d'exécution
container_status() {
    if docker ps | grep coralplanters_db >/dev/null; then
        echo "up"
    else
        echo "down"
    fi
}

# Fonction pour sauvegarder la base de données
backup_database() {
    echo "Entrez le nom de fichier pour la sauvegarde SQL ou laissez vide pour le nom par défaut (backup.sql):"
    read -r filename

    # Obtenir la date et l'heure actuelles dans un format français complet (par exemple, 12-Avril-2023_11h32m12s)
    current_date_time=$(LC_TIME="fr_FR.UTF-8" date "+%d-%B-%Y_%Hh%Mm%Ss")

    # Créer le dossier de sauvegarde s'il n'existe pas
    backup_dir="./documents/database/backups"
    mkdir -p "$backup_dir"

    # Utiliser le nom de fichier entré par l'utilisateur, ou utiliser un nom par défaut basé sur la date et l'heure actuelles
    filename=${filename:-"sauvegarde_${current_date_time}.sql"}

    # Ajouter le chemin du dossier de sauvegarde au nom de fichier
    filename="${backup_dir}/${filename}"

    # Sauvegarder la base de données dans le fichier spécifié par l'utilisateur ou le nom par défaut
    docker exec database-coralplanters_db-1 pg_dump -U coralplanters >"$filename"

    echo "La base de données a été sauvegardée dans le fichier $filename."
}

# Afficher l'état du conteneur Docker
status=$(container_status)
echo "Le conteneur Docker est actuellement $status."

# Fonction de nettoyage en cas d'interruption par Ctrl+C
cleanup() {
    echo ""
    echo "Au revoir !"
    echo ""
    exit 0
}

# Capturer l'interruption par Ctrl+C pour nettoyer correctement le script
trap cleanup INT

# Boucle jusqu'à ce que l'utilisateur entre 1, 2, 3 ou tape exit ou q
while true; do
    # Afficher le menu de sélection
    echo "Bienvenue sur le script de gestion de la base de données Coral Planters !"
    echo "Que souhaitez-vous faire ?"

    # Afficher les options de menu en fonction de l'état du conteneur Docker
    if [[ $status == "down" ]]; then
        echo "1. Démarrer le conteneur Docker"
        echo "2. Quitter"
    elif [[ $status == "up" ]]; then
        echo "1. Arrêter le conteneur Docker"
        echo "2. Sauvegarder la base de données"
        echo "3. Quitter"
    fi

    read -p "Votre choix : " choice

    # Exécuter l'action en fonction de l'option de menu sélectionnée
    case $choice in
    # Démarrer le conteneur Docker si l'état actuel est "down" (éteint)
    1)
        if [[ $status == "down" ]]; then
            # Démarrer le conteneur Docker
            echo "Démarrage du conteneur Docker..."
            # Démarrer le conteneur Docker en arrière-plan avec le fichier de configuration docker-compose.yml
            docker-compose -f "$COMPOSE_FILE" up -d >/dev/null # Rediriger la sortie vers null pour masquer la sortie standard
            # Vérifier si le conteneur est en cours d'exécution
            if docker ps | grep coralplanters_db >/dev/null; then
                echo "Le conteneur Docker a démarré avec succès."
                status=$(container_status)
            else
                echo "Erreur: Impossible de démarrer le conteneur Docker."
                exit 1
            fi
        # Arrêter le conteneur Docker si l'état actuel est "up" (allumé)
        elif [[ $status == "up" ]]; then
            # Arrêter le conteneur Docker
            echo "Arrêt du conteneur Docker..."
            # Arrêter le conteneur Docker avec le fichier de configuration docker-compose.yml
            docker-compose -f "$COMPOSE_FILE" down >/dev/null # Rediriger la sortie vers null pour masquer la sortie standard
            # Vérifier si le conteneur a été arrêté
            if ! docker ps | grep coralplanters_db >/dev/null; then
                echo "Le conteneur Docker a été arrêté avec succès."
                status=$(container_status)
            else
                echo "Erreur: Impossible d'arrêter le conteneur Docker."
                exit 1
            fi
        fi
        ;;

    2)
        # Sauvegarder la base de données si l'état actuel est "up" (allumé)
        if [[ $status == "down" ]]; then
            # Quitter le script
            echo ""
            echo "Au revoir !"
            echo ""
            exit 0
        elif [[ $status == "up" ]]; then
            # Sauvegarder la base de données
            backup_database
        fi
        ;;

    3)
        # Quitter le script si l'état actuel est "up" (allumé)
        if [[ $status == "up" ]]; then
            # Quitter le script
            echo ""
            echo "Au revoir !"
            echo ""
            exit 0
        fi
        ;;
    q | exit)
        # Quitter le script
        echo ""
        echo "Au revoir !"
        echo ""
        exit 0
        ;;
    *)
        # Si l'utilisateur entre autre chose que 1, 2, 3, q ou exit
        echo "Erreur: Veuillez entrer 1, 2, 3, q ou exit."
        ;;
    esac
done
