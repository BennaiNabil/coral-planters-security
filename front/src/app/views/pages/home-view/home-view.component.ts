import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home-view',
  templateUrl: './home-view.component.html',
  styleUrls: ['./home-view.component.css']
})
export class HomeViewComponent implements OnInit {
  cards = [
    {
      image: 'https://placehold.co/600x400',
      title: 'Restore Coral Reefs',
      description: 'Help us restore coral reefs by donating to our cause.',
    },
    {
      image: 'https://placehold.co/600x400.jpg',
      title: 'Sponsor a Coral',
      description: 'Sponsor a coral and help us replant damaged reefs.',
    },
    {
      image: 'https://placehold.co/600x400.jpg',
      title: 'Volunteer for Reef Cleanup',
      description: 'Join us for a reef cleanup and help us remove harmful debris.',
    },
    {
      image: 'https://placehold.co/600x400.jpg',
      title: 'Educate the Community',
      description: 'Help us raise awareness about the importance of coral reef conservation.',
    },
    {
      image: 'https://placehold.co/600x400.jpg',
      title: 'Partner with Us',
      description: 'Partner with us to help fund our ongoing coral restoration efforts.',
    },
  ];

  constructor() {
  }

  ngOnInit(): void {
  }


}
