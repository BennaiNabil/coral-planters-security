import {Component, OnInit} from '@angular/core';
import {RoleService} from "../../../../../services/role.service";
import {Role} from "../../../../../models/Role";
import {EditRoleDialogComponent} from "../../../../components/dialogs/edit-role-dialog/edit-role-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-role-admin-view',
  templateUrl: './role-admin-view.component.html',
  styleUrls: ['./role-admin-view.component.css']
})
export class RoleAdminViewComponent implements OnInit {

  roles: Role[] = [];
  displayedColumns: string[] = ['id', 'role', 'actions'];


  constructor(private roleService: RoleService, public dialog: MatDialog, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.loadRoles();
  }

  loadRoles(): void {
    this.roleService.getAll().subscribe((roles: Role[]) => {
      this.roles = roles;
    });
  }

  editRole(role: Role): void {
    const dialogRef = this.dialog.open(EditRoleDialogComponent, {
      width: '400px',
      data: {role: role}
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        role.id = result.id;
        role.role = result.role;
      }
    });

  }

  deleteRole(role: Role): void {
    const confirmed: boolean = window.confirm(`Êtes-vous sûr de vouloir supprimer le rôle ${role.role}?`);
    if (confirmed) {
      this.roleService.delete(role.id).subscribe(() => {
        this.snackBar.open(`Rôle ${role.role}  supprimé avec succès`, 'Fermer', {
          duration: 3000,
        });
        this.loadRoles();
      });
    }
  }

}
