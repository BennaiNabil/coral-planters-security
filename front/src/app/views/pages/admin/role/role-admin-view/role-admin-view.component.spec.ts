import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RoleAdminViewComponent } from './role-admin-view.component';
import { RoleService } from '../../../../../services/role.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { Role } from '../../../../../models/Role';
import {ReactiveFormsModule} from "@angular/forms";

describe('RoleAdminViewComponent', () => {
  let component: RoleAdminViewComponent;
  let fixture: ComponentFixture<RoleAdminViewComponent>;
  let roleService: RoleService;
  let dialog: MatDialog;
  let snackBar: MatSnackBar;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RoleAdminViewComponent],
      providers: [RoleService],
      imports: [
        HttpClientTestingModule,
        MatDialogModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        ReactiveFormsModule
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleAdminViewComponent);
    component = fixture.componentInstance;
    roleService = TestBed.inject(RoleService);
    dialog = TestBed.inject(MatDialog);
    snackBar = TestBed.inject(MatSnackBar);
    fixture.detectChanges();
  });

  // Vérifie si le composant est créé correctement
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // Vérifie si la méthode loadRoles() fonctionne correctement
  it('should load roles', () => {
    spyOn(roleService, 'getAll').and.returnValue(
      of([
        { id: 1, role: 'Admin' },
        { id: 2, role: 'User' },
      ] as Role[])
    );

    component.loadRoles();

    expect(roleService.getAll).toHaveBeenCalled();
    expect(component.roles.length).toBe(2);
  });

  // Vérifie si la méthode editRole() ouvre la boîte de dialogue
  it('should open the edit role dialog', () => {
    const role = { id: 1, role: 'Admin' } as Role;
    spyOn(dialog, 'open').and.callThrough();

    component.editRole(role);

    expect(dialog.open).toHaveBeenCalled();
  });

  // Vérifie si la méthode deleteRole() supprime le rôle et affiche le message de succès
  it('should delete the role and show success message', () => {
    const role = { id: 1, role: 'Admin' } as Role;
    spyOn(window, 'confirm').and.returnValue(true);
    spyOn(roleService, 'delete').and.returnValue(of({}));
    spyOn(snackBar, 'open').and.callThrough();
    spyOn(component, 'loadRoles').and.callThrough();

    component.deleteRole(role);

    expect(roleService.delete).toHaveBeenCalledWith(role.id);
    expect(snackBar.open).toHaveBeenCalled();
    expect(component.loadRoles).toHaveBeenCalled();
  });
});
