import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  isLoggedIn: boolean = false;
  menuOpen: boolean = false;
  isMobile: boolean = true;


  constructor() {
  }

  ngOnInit(): void {
    this.isLoggedIn = false;
    this.checkIsMobile();
    window.addEventListener('resize', () => this.checkIsMobile());
  }

  onLogout(): void {
    alert("Logout");
    this.isLoggedIn = false;
  }


  onLogin(): void {
    alert("Login");
    this.isLoggedIn = true;
  }

  toggleMenu(): void {
    this.menuOpen = !this.menuOpen;
  }


  private checkIsMobile() {
    this.isMobile = window.innerWidth < 600;
  }

}
