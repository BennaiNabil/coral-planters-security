import {ComponentFixture, TestBed} from '@angular/core/testing';
import {EditRoleDialogComponent} from './edit-role-dialog.component';
import {FormBuilder} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {RoleService} from '../../../../services/role.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Role} from '../../../../models/Role';
import {of} from 'rxjs';

describe('EditRoleDialogComponent', () => {
  let component: EditRoleDialogComponent;
  let fixture: ComponentFixture<EditRoleDialogComponent>;
  let roleService: RoleService;
  let dialogRefSpy: MatDialogRef<EditRoleDialogComponent>;

  beforeEach(async () => {
    dialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);

    await TestBed.configureTestingModule({
      declarations: [EditRoleDialogComponent],
      providers: [
        FormBuilder,
        {provide: MatDialogRef, useValue: dialogRefSpy},
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            role: {id: 1, role: 'Admin'},
          },
        },
        RoleService,
      ],
      imports: [HttpClientTestingModule, BrowserAnimationsModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRoleDialogComponent);
    component = fixture.componentInstance;
    roleService = TestBed.inject(RoleService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should save role on saveRole()', () => {
    spyOn(roleService, 'update').and.returnValue(
      of({id: 1, role: 'New Admin'} as Role)
    );

    component.roleForm.patchValue({role: 'New Admin'});

    component.saveRole();

    expect(roleService.update).toHaveBeenCalledWith({
      id: 1,
      role: 'New Admin',
    });
    expect(dialogRefSpy.close).toHaveBeenCalledOnceWith({
      id: 1,
      role: 'New Admin',
    });
  });

  it('should not save role if form is invalid', () => {
    spyOn(roleService, 'update');
    // spyOn(dialogRefSpy, 'close');

    component.roleForm.patchValue({role: ''});

    component.saveRole();

    expect(roleService.update).not.toHaveBeenCalled();
    expect(dialogRefSpy.close).not.toHaveBeenCalled();
  });
});
