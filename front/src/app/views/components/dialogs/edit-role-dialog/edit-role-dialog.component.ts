import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {RoleService} from "../../../../services/role.service";

@Component({
  selector: 'app-edit-role-dialog',
  templateUrl: './edit-role-dialog.component.html',
  styleUrls: ['./edit-role-dialog.component.css']
})
export class EditRoleDialogComponent implements OnInit {

  roleForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditRoleDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private roleService: RoleService
  ) {
    this.roleForm = this.fb.group({
      id: [{value: data.role.id, disabled: true}, Validators.required],
      role: [data.role.role, Validators.required]
    });
  }

  saveRole(): void {
    if (this.roleForm.valid) {
      this.roleService.update(this.roleForm.getRawValue()).subscribe(() => {
        this.dialogRef.close(this.roleForm.getRawValue());
      });
    }
  }


  ngOnInit(): void {
  }

}
