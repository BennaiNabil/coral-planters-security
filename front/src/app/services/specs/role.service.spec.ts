// Importer les classes Angular nécessaires pour les tests
import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RoleService} from "../role.service";
import {HttpClientModule} from "@angular/common/http";

// Décrire les tests pour le service RoleService
describe('RoleService', () => {
  let httpMock: HttpTestingController; // Le mock pour simuler les requêtes HTTP
  let service: RoleService; // L'instance du service à tester

  // Initialiser les composants nécessaires avant chaque test
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule], // Importer les modules HttpClient nécessaires
      providers: [RoleService] // Ajouter le service à tester comme fournisseur de dépendances
    });
    httpMock = TestBed.inject(HttpTestingController); // Récupérer le mock pour les requêtes HTTP
    service = TestBed.inject(RoleService); // Récupérer l'instance du service à tester
  });

  // Vérifier que toutes les requêtes HTTP ont été effectuées correctement après chaque test
  afterEach(() => {
    httpMock.verify();
  });

  // Vérifier que le service a été créé
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // Vérifier que le service appelle l'API pour récupérer tous les rôles
  it('should call the API to get all roles', () => {
    const dummyRoles = [
      {id: 1, role: 'admin'},
      {id: 2, role: 'user'}
    ];

    service.getAll().subscribe(roles => {
      expect(roles.length).toBe(2); // Vérifier que le service renvoie deux rôles
      expect(roles).toEqual(dummyRoles); // Vérifier que les rôles renvoyés sont les mêmes que les rôles fictifs
    });

    const req = httpMock.expectOne(`${service.getBase()}`); // Vérifier que le service appelle l'API à la bonne URL
    expect(req.request.method).toBe('GET'); // Vérifier que le service appelle la méthode HTTP GET
    req.flush(dummyRoles); // Simuler la réponse de l'API avec les rôles fictifs
  });

  // Vérifier que le service appelle l'API pour récupérer un rôle par son identifiant
  it('should call the API to get a role by id', () => {
    const dummyRole = {id: 1, role: 'admin'};

    service.getById(1).subscribe(role => {
      expect(role).toEqual(dummyRole); // Vérifier que le rôle renvoyé est le même que le rôle fictif
    });

    const req = httpMock.expectOne(`${service.getBase()}/1`); // Vérifier que le service appelle l'API à la bonne URL
    expect(req.request.method).toBe('GET'); // Vérifier que le service appelle la méthode HTTP GET
    req.flush(dummyRole); // Simuler la réponse de l'API avec le rôle fictif
  });

// Vérifier que le service appelle l'API pour ajouter un rôle
  it('should call the API to add a role', () => {
    const dummyRole = {id: 1, role: 'admin'};

    service.add(dummyRole).subscribe(res => {
      expect(res).toEqual(dummyRole); // Vérifier que la réponse renvoyée par le service est le même que le rôle fictif
    });

    const req = httpMock.expectOne(`${service.getBase()}`); // Vérifier que le service appelle l'API à la bonne URL
    expect(req.request.method).toBe('POST'); // Vérifier que le service appelle la méthode HTTP POST
    req.flush(dummyRole); // Simuler la réponse de l'API avec le rôle fictif
  });

  // Vérifier que le service appelle l'API pour mettre à jour un rôle
  it('should call the API to update a role', () => {
    const dummyRole = {id: 1, role: 'admin'};

    service.update(dummyRole).subscribe(res => {
      expect(res).toEqual(dummyRole); // Vérifier que la réponse renvoyée par le service est le même que le rôle fictif
    });

    const req = httpMock.expectOne(`${service.getBase()}`); // Vérifier que le service appelle l'API à la bonne URL
    expect(req.request.method).toBe('PUT'); // Vérifier que le service appelle la méthode HTTP PUT
    req.flush(dummyRole); // Simuler la réponse de l'API avec le rôle fictif
  });

  // Vérifier que le service appelle l'API pour supprimer un rôle
  it('should call the API to delete a role', () => {
    service.delete(1).subscribe(res => {
      expect(res).toBeNull(); // Vérifier que la réponse renvoyée par le service est nulle
    });

    const req = httpMock.expectOne(`${service.getBase()}/1`); // Vérifier que le service appelle l'API à la bonne URL
    expect(req.request.method).toBe('DELETE'); // Vérifier que le service appelle la méthode HTTP DELETE
    req.flush(null); // Simuler la réponse de l'API avec une réponse nulle
  });

});

