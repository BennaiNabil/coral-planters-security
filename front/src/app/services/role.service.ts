import {Injectable} from '@angular/core';
import {CrudService} from './crud.service';
import {Role} from "../models/Role";

@Injectable({
  providedIn: 'root'
})
export class RoleService extends CrudService<Role> {
  base_service: string = "/role";
}
