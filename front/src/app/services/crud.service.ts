import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export abstract class CrudService<T> {

  readonly SUFFIXE = "";
  abstract base_service: string;

  protected constructor(protected http: HttpClient) {
  }

  getById(rId: Number): Observable<T> {
    return this.http.get<T>(`${this.getBase()}/${rId}${this.SUFFIXE}`);
  }

  getAll(): Observable<T[]> {
    return this.http.get<T[]>(`${this.getBase()}${this.SUFFIXE}`);
  }

  add(rObject: T): Observable<any> {
    return this.http.post<T>(`${this.getBase()}${this.SUFFIXE}`, rObject);
  }

  update(rObject: T): Observable<any> {
    return this.http.put<T>(`${this.getBase()}${this.SUFFIXE}`, rObject);
  }

  delete(rId: number): Observable<any> {
    return this.http.delete<T>(`${this.getBase()}/${rId}${this.SUFFIXE}`);
  }

  getBase(): string {
    return environment.BASE_API + this.base_service;
  }


}
