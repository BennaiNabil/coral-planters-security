import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeViewComponent} from "./views/pages/home-view/home-view.component";
import {RoleAdminViewComponent} from "./views/pages/admin/role/role-admin-view/role-admin-view.component";

const routes: Routes = [
  {path: '', component: HomeViewComponent},
  {path: 'home', component: HomeViewComponent},
  {path: 'admin/role', component: RoleAdminViewComponent},
  {path: 'about', component: HomeViewComponent},
  {path: 'contact', component: HomeViewComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
