# Guidelines pour les développeurs front-end de Coral Planters

<!-- TOC -->

* [Guidelines pour les développeurs front-end de Coral Planters](#guidelines-pour-les-développeurs-front-end-de-coral-planters)
  * [Services](#services)
    * [Utilisez les décorateurs Angular appropriés](#utilisez-les-décorateurs-angular-appropriés)
    * [Héritez de CrudService](#héritez-de-crudservice)
    * [Définissez l'URL de base pour le service](#définissez-lurl-de-base-pour-le-service)
    * [Importez les classes requises](#importez-les-classes-requises)
    * [Personnalisez le service selon les besoins](#personnalisez-le-service-selon-les-besoins)
    * [Assurez la couverture de tests](#assurez-la-couverture-de-tests)
  * [Tests unitaires](#tests-unitaires)
    * [Structure générale](#structure-générale)
    * [Exemples de tests unitaires](#exemples-de-tests-unitaires)
    * [Bonnes pratiques](#bonnes-pratiques)

<!-- TOC -->

## Services

Lors de l'écriture de services Angular qui héritent de CrudService, il est important de suivre certaines bonnes
pratiques et principes pour assurer une architecture propre et maintenable. Voici quelques guidelines à suivre :

### Utilisez les décorateurs Angular appropriés

Utilisez le décorateur @Injectable pour marquer la classe comme un service pouvant être injecté comme une dépendance.

```typescript
@Injectable({
  providedIn: 'root'
})
```

### Héritez de CrudService

Étendez la classe CrudService avec le type de modèle approprié pour tirer parti des fonctionnalités de base fournies par
le service générique.

```typescript
export class RoleService extends CrudService<Role> {
// ...
}
```

### Définissez l'URL de base pour le service

Définissez l'URL de base pour le service en définissant une propriété base_service. Cette URL sera utilisée par
CrudService pour construire les URL des requêtes HTTP.

```typescript
base_service: string = "/role";
```

### Importez les classes requises

Importez les classes nécessaires pour le service, telles que les modèles ou les autres services.

```typescript
import {Injectable} from '@angular/core';
import {CrudService} from './crud.service';
import {Role} from "../models/Role";
```

### Personnalisez le service selon les besoins

Si vous devez ajouter ou modifier des fonctionnalités spécifiques au service, vous pouvez le faire en ajoutant ou en
surchargeant les méthodes dans la classe de service. Cela permet de garder la logique spécifique au service séparée de
la logique générique de CrudService.

### Assurez la couverture de tests

Écrivez des tests unitaires pour le service, en suivant les guidelines pour les tests unitaires. Assurez-vous de tester
les scénarios spécifiques à votre service et de vérifier que la logique de CrudService fonctionne correctement avec
votre modèle.

## Tests unitaires

Voici quelques guidelines pour écrire des tests unitaires pour votre équipe en utilisant Angular et le fichier fourni.

### Structure générale

- Imports: Importez les classes nécessaires pour les tests, y compris celles pour les tests Angular et les classes
  spécifiques à votre projet.

```typescript
import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RoleService} from "../role.service";
import {HttpClientModule} from "@angular/common/http";

```

- Describe: Utilisez la fonction describe() pour regrouper les tests unitaires relatifs à un service ou un composant
  spécifique.

```typescript
describe('RoleService', () => {
  let httpMock: HttpTestingController; // Le mock pour simuler les requêtes HTTP
  let service: RoleService; // L'instance du service à tester
});
```

- BeforeEach: Utilisez la fonction beforeEach() pour initialiser les composants nécessaires avant chaque test, y compris
  les modules et les services.

```typescript
  // Initialiser les composants nécessaires avant chaque test
beforeEach(() => {
  TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, HttpClientModule], // Importer les modules HttpClient nécessaires
    providers: [RoleService] // Ajouter le service à tester comme fournisseur de dépendances
  });
  httpMock = TestBed.inject(HttpTestingController); // Récupérer le mock pour les requêtes HTTP
  service = TestBed.inject(RoleService); // Récupérer l'instance du service à tester
});

```

- AfterEach: Utilisez la fonction afterEach() pour vérifier que toutes les requêtes HTTP ont été effectuées correctement
  après chaque test.

```typescript
  // Vérifier que toutes les requêtes HTTP ont été effectuées correctement après chaque test
afterEach(() => {
  httpMock.verify();
});
```

### Exemples de tests unitaires

Voici quelques exemples de tests unitaires pour le service RoleService :

- Test de création: Vérifiez que le service a été créé en utilisant toBeTruthy().

```typescript
  // Vérifier que le service a été créé
it('should be created', () => {
  expect(service).toBeTruthy();
});
```

- Test de récupération: Vérifiez que le service appelle l'API pour récupérer des données (par exemple, tous les rôles ou
  un rôle spécifique) et comparez les données renvoyées avec les données attendues.

```ts

// Vérifier que le service appelle l'API pour récupérer tous les rôles
it('should call the API to get all roles', () => {
  const dummyRoles = [
    {id: 1, name: 'admin'},
    {id: 2, name: 'user'}
  ];

  service.getAll().subscribe(roles => {
    expect(roles.length).toBe(2); // Vérifier que le service renvoie deux rôles
    expect(roles).toEqual(dummyRoles); // Vérifier que les rôles renvoyés sont les mêmes que les rôles fictifs
  });

  const req = httpMock.expectOne(`${service.getBase()}`); // Vérifier que le service appelle l'API à la bonne URL
  expect(req.request.method).toBe('GET'); // Vérifier que le service appelle la méthode HTTP GET
  req.flush(dummyRoles); // Simuler la réponse de l'API avec les rôles fictifs
});
```

- Test d'ajout: Vérifiez que le service appelle l'API pour ajouter un nouvel élément et comparez la réponse renvoyée
  avec les données attendues.

```typescript
// Vérifier que le service appelle l'API pour ajouter un rôle
it('should call the API to add a role', () => {
  const dummyRole = {id: 1, name: 'admin'};

  service.add(dummyRole).subscribe(res => {
    expect(res).toEqual(dummyRole); // Vérifier que la réponse renvoyée par le service est le même que le rôle fictif
  });

  const req = httpMock.expectOne(`${service.getBase()}`); // Vérifier que le service appelle l'API à la bonne URL
  expect(req.request.method).toBe('POST'); // Vérifier que le service appelle la méthode HTTP POST
  req.flush(dummyRole); // Simuler la réponse de l'API avec le rôle fictif
});
```

- Test de mise à jour: Vérifiez que le service appelle l'API pour mettre à jour un élément existant et comparez la
  réponse renvoyée avec les données attendues.

```typescript
// Vérifier que le service appelle l'API pour mettre à jour un rôle
it('should call the API to update a role', () => {
  const dummyRole = {id: 1, name: 'admin'};

  service.update(dummyRole).subscribe(res => {
    expect(res).toEqual(dummyRole); // Vérifier que la réponse renvoyée par le service est le même que le rôle fictif
  });

  const req = httpMock.expectOne(`${service.getBase()}`); // Vérifier que le service appelle l'API à la bonne URL
  expect(req.request.method).toBe('PUT'); // Vérifier que le service appelle la méthode HTTP PUT
  req.flush(dummyRole); // Simuler la réponse de l'API avec le rôle fictif
});
```

- Test de suppression: Vérifiez que le service appelle l'API pour supprimer un élément et vérifiez que la réponse
  renvoyée est nulle.

```typescript
// Vérifier que le service appelle l'API pour supprimer un rôle
it('should call the API to delete a role', () => {
  service.delete(1).subscribe(res => {
    expect(res).toBeNull(); // Vérifier que la réponse renvoyée par le service est nulle
  });

  const req = httpMock.expectOne(`${service.getBase()}/1`); // Vérifier que le service appelle l'API à la bonne URL
  expect(req.request.method).toBe('DELETE'); // Vérifier que le service appelle la méthode HTTP DELETE
  req.flush(null); // Simuler la réponse de l'API avec une réponse nulle
});

```

### Bonnes pratiques

- Utilisez des noms de tests descriptifs pour décrire clairement ce que chaque test vérifie.
- Assurez-vous que vos tests sont indépendants les uns des autres et n'ont pas d'effets de bord.
- Utilisez des données fictives (dummy data) pour simuler les réponses de l'API et valider le comportement de votre
  service.
- Testez tous les scénarios possibles, y compris les cas limites et les erreurs.
- En suivant ces guidelines et en utilisant le fichier fourni comme base, votre équipe devrait être en mesure d'écrire
  des tests unitaires efficaces et maintenables pour vos services et composants Angular.


