//package org.simplon.coralplanters.controllers;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.simplon.coralplanters.models.Role;
//import org.simplon.coralplanters.models.RoleEnum;
//import org.simplon.coralplanters.services.RoleService;
//
//import java.util.Arrays;
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.when;
//
//class RoleControllerTest {
//
//    @Mock
//    private RoleService roleService;
//
//    @InjectMocks
//    private RoleController roleController;
//
//    private Role role1;
//    private Role role2;
//
//    @BeforeEach
//    void setUp() {
//        MockitoAnnotations.initMocks(this);
//        role1 = new Role();
//        role1.setId(1L);
//        role1.setRole(RoleEnum.ADMIN);
//
//        role2 = new Role();
//        role2.setId(2L);
//        role2.setRole(RoleEnum.USER);
//    }
//
//    @Test
//    void testFindAllShouldReturnAllRoles() {
//        List<Role> roles = Arrays.asList(role1, role2);
//        when(roleService.findAll()).thenReturn(roles);
//
//        List<Role> result = roleController.findAll();
//
//        assertEquals(2, result.size());
//        assertEquals("ROLE_ADMIN", result.get(0).getRole().name());
//        assertEquals("ROLE_USER", result.get(1).getRole().name());
//    }
//
//    @Test
//    void testFindByIdShouldReturnRoleWhenFound() {
//        when(roleService.findById(1L)).thenReturn(role1);
//
//        Role result = roleController.findById(1L);
//
//        assertEquals("ROLE_ADMIN", result.getRole());
//    }
//
//    @Test
//    void testFindByRoleNameShouldReturnRoleWhenFound() {
//        when(roleService.findByRole(RoleEnum.USER)).thenReturn(role2);
//
//        Role result = roleController.findByRoleName("USER");
//
//        assertEquals(2L, result.getId());
//    }
//
//    @Test
//    void testUpdateShouldReturnUpdatedRole() {
//        Role updatedRole = new Role();
//        updatedRole.setId(1L);
//        updatedRole.setRole(RoleEnum.ADMIN);
//
//        when(roleService.save(updatedRole)).thenReturn(updatedRole);
//
//        Role result = roleController.update(updatedRole);
//
//        assertEquals("ROLE_SUPER_ADMIN", result.getRole());
//    }
//
//    @Test
//    void testSaveShouldReturnSavedRole() {
//        when(roleService.save(role1)).thenReturn(role1);
//
//        Role result = roleController.save(role1);
//
//        assertEquals("ROLE_ADMIN", result.getRole());
//    }
//
//}
