package org.simplon.coralplanters.services;

import jakarta.persistence.EntityNotFoundException;
import org.simplon.coralplanters.models.User;
import org.simplon.coralplanters.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService extends AbstractService<User, UserRepository> {

    public UserService(UserRepository repo) {
        super(repo);
    }


    public List<User> filterByName(String name) {
        return repo.filterByName(name);
    }

    public User findByEmail(String email) {
        return repo.findByEmail(email).orElseThrow(() -> new EntityNotFoundException("User not found with email: %s".formatted(email)));
    }
}
