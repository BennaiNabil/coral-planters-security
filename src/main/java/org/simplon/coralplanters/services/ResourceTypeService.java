package org.simplon.coralplanters.services;

import jakarta.persistence.EntityNotFoundException;
import org.simplon.coralplanters.models.ResourceType;
import org.simplon.coralplanters.repositories.ResourceTypeRepository;
import org.springframework.stereotype.Service;

@Service
public class ResourceTypeService extends AbstractService<ResourceType, ResourceTypeRepository> {

    public ResourceTypeService(ResourceTypeRepository repo) {
        super(repo);
    }


    public ResourceType findByResourcetype(String resourceTypeName) {
        return repo.findByResourcetypeIgnoreCase(resourceTypeName)
            .orElseThrow(() -> new EntityNotFoundException("ResourceType not found with name: %s".formatted(resourceTypeName)));
    }
}
