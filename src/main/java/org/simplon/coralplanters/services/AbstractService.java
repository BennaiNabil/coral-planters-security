package org.simplon.coralplanters.services;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * The type Abstract service.
 *
 * @param <T>     the type parameter
 * @param <R> the type parameter
 */
@Service
public abstract class AbstractService<T, R extends JpaRepository<T, Long>> {

    /**
     * The Repo.
     */
    protected R repo;

    private String entityClassName;


    /**
     * Instantiates a new Abstract service.
     *
     * @param repo the repo
     */
    protected AbstractService(R repo) {
        this.repo = repo;
        // RPZ la veille de Matthias sur la réflexivité qui prend des stéroïdes
        entityClassName = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getSimpleName();
    }

    /**
     * Find the list of all entities.
     *
     * @return the list
     */
    public List<T> findAll() {
        return repo.findAll();
    }

    /**
     * Find the entity by id.
     *
     * @param id the id
     * @return the t
     */
    public T findById(Long id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("%s not found with id: %d".formatted(entityClassName, id)));
    }

    /**
     * Update the entity.
     *
     * @param entity the entity
     * @return the t
     */
    public T update(T entity) {
        return repo.save(entity);
    }

    /**
     * Save the entity.
     *
     * @param entity the entity
     * @return the t
     */
    public T save(T entity) {
        return repo.save(entity);
    }

    /**
     * Delete the entity by id.
     *
     * @param id the id
     */
    public void deleteById(Long id) {
        repo.deleteById(id);
    }

    /**
     * Delete the entity.
     *
     * @param entity the entity
     */
    public void delete(T entity) {
        repo.delete(entity);
    }


}
