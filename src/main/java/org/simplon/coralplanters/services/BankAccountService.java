package org.simplon.coralplanters.services;

import org.simplon.coralplanters.models.BankAccount;
import org.simplon.coralplanters.repositories.BankAccountRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankAccountService extends AbstractService<BankAccount, BankAccountRepository> {

    public BankAccountService(BankAccountRepository repo) {
        super(repo);
    }

    public List<BankAccount> findByUserId(Long userId) {
        return repo.findByUserId(userId);
    }
}
