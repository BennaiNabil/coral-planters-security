package org.simplon.coralplanters.services;

import org.simplon.coralplanters.models.Address;
import org.simplon.coralplanters.repositories.AddressRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService extends AbstractService<Address, AddressRepository> {

    public AddressService(AddressRepository repo) {
        super(repo);
    }


    public List<Address> findByCountry(String country) {
        return this.repo.findByCountry(country);
    }

    public List<Address> findByCity(String city) {
        return this.repo.findByCity(city);
    }
}
