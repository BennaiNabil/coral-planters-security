package org.simplon.coralplanters.services;

import jakarta.persistence.EntityNotFoundException;
import org.simplon.coralplanters.models.Role;
import org.simplon.coralplanters.models.RoleEnum;
import org.simplon.coralplanters.repositories.RoleRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleService extends AbstractService<Role, RoleRepository> {

    public RoleService(RoleRepository repo) {
        super(repo);
    }

    public Role findByRole(RoleEnum roleEnum) {
        return repo.findByRoleIgnoreCase(roleEnum.name())
                   .orElseThrow(
                       () -> new EntityNotFoundException("Role not found with name: %s".formatted(roleEnum.name())));
    }
}
