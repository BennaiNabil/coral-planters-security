package org.simplon.coralplanters.models;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idrole", nullable = false)
    private Long id;

    @Enumerated(EnumType.STRING)
    private RoleEnum role;

    public Role(RoleEnum pRole) {
        role = pRole;
    }

}
