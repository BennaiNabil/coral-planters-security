package org.simplon.coralplanters.models;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "resource")
public class Resource {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idresource", nullable = false)
    private Long id;

    @Column(name = "url", nullable = false)
    private String url;


    @Column(name = "uploadedat", nullable = false)
    private LocalDateTime uploadedAt;

    @ManyToOne
    @JoinColumn(name = "resourcetype_id", foreignKey = @ForeignKey(name = "fk_resource_resourcetype"))
    private ResourceType resourceType;


    @ManyToOne
    @JoinColumn(name = "coral_id", foreignKey = @ForeignKey(name = "fk_resource_coral"))
    private Coral coral;

    public Resource(String pUrl, LocalDateTime pUploadedAt, ResourceType pResourceType, Coral pCoral) {
        url = pUrl;
        uploadedAt = pUploadedAt;
        resourceType = pResourceType;
        coral = pCoral;
    }
}