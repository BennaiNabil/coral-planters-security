package org.simplon.coralplanters.models;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "'group'")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idgroup", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "createdat", nullable = false)
    private LocalDateTime createdAt = LocalDateTime.now();

    @Column(name = "closedat", nullable = true)
    private LocalDateTime closedAt;

    @ManyToOne
    @JoinColumn(name = "creator_id", foreignKey = @ForeignKey(name = "fk_group_creator"))
    private User creator;

    @ManyToOne
    @JoinColumn(name = "admin_id", foreignKey = @ForeignKey(name = "fk_group_admin"))
    private User admin;

    @ManyToOne
    @JoinColumn(name = "structure_id", foreignKey = @ForeignKey(name = "fk_group_structure"))
    private Structure structure;

    @ManyToMany
    @JoinTable(name = "group_user", joinColumns = @JoinColumn(name = "group_id", foreignKey = @ForeignKey(name = "fk_group_user_group")), inverseJoinColumns = @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "fk_group_user_user")))
    private List<User> users;

    public Group(String pName, LocalDateTime pCreatedAt, LocalDateTime pClosedAt, User pCreator, User pAdmin, Structure pStructure, List<User> pUsers) {
        name = pName;
        createdAt = pCreatedAt;
        closedAt = pClosedAt;
        creator = pCreator;
        admin = pAdmin;
        structure = pStructure;
        users = pUsers;
    }
}