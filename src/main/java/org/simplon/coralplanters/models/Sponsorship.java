package org.simplon.coralplanters.models;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;


@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "sponsorship")
public class Sponsorship {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idsponsorship", nullable = false)
    private Long id;

    @Column(name = "amount", nullable = false)
    private Integer amount;

    @Column(name = "startedat", nullable = false)
    private LocalDateTime startedat;

    @ManyToOne
    @JoinColumn(name = "sponsor_id", foreignKey = @ForeignKey(name = "fk_sponsorship_user"))
    private User sponsor;

    @ManyToOne
    @JoinColumn(name = "coral_id", foreignKey = @ForeignKey(name = "fk_sponsorship_coral"))
    private Coral coral;


    public Sponsorship(Integer pAmount, LocalDateTime pStartedat, User pSponsor, Coral pCoral) {
        amount = pAmount;
        startedat = pStartedat;
        sponsor = pSponsor;
        coral = pCoral;
    }
}