package org.simplon.coralplanters.models;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Cascade;

import java.time.LocalDateTime;
import java.util.List;

import static org.hibernate.annotations.CascadeType.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "'user'")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iduser", nullable = false)
    private Long id;

    @Column(name = "firstname", nullable = false)
    private String firstname;

    @Column(name = "lastname", nullable = false)
    private String lastname;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "createdat", nullable = false)
    private LocalDateTime createdAt = LocalDateTime.now();

    @Column(name = "bannedat", nullable = true)
    private LocalDateTime bannedAt;

    @Column(name = "phonenumber", nullable = false)
    private String phoneNumber;

    @ManyToOne
    @JoinColumn(name = "role_id", foreignKey = @ForeignKey(name = "fk_user_role"))
    private Role role;

    @ManyToMany
    @JoinTable(name = "user_address", joinColumns = @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "fk_user_address_user")), inverseJoinColumns = @JoinColumn(name = "address_id", foreignKey = @ForeignKey(name = "fk_user_address_address")))
    @Cascade(ALL)
    private List<Address> address;

    public User(String pFirstname, String pLastname, String pEmail, String pPassword, LocalDateTime pCreatedAt, LocalDateTime pBannedAt, String pPhoneNumber, Role pRole, List<Address> pAddress) {
        firstname = pFirstname;
        lastname = pLastname;
        email = pEmail;
        password = pPassword;
        createdAt = pCreatedAt;
        bannedAt = pBannedAt;
        phoneNumber = pPhoneNumber;
        role = pRole;
        address = pAddress;
    }
}
