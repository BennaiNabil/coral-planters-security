package org.simplon.coralplanters.models;

import jakarta.persistence.*;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "address")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idaddress", nullable = false)
    private Long id;

    @Column(name = "longitude", nullable = false)
    private Double longitude;

    @Column(name = "latitude", nullable = false)
    private Double latitude;

    @Column(name = "country", nullable = false)
    private String country;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "zipcode", nullable = false)
    private String zipcode;

    @Column(name = "isprimaryaddress", nullable = false)
    private Boolean isPrimaryAddress = false;

    public Address(Double pLongitude, Double pLatitude) {
        longitude = pLongitude;
        latitude = pLatitude;
    }

    public Address(String pCountry, String pCity, String pAddress, String pZipcode) {
        country = pCountry;
        city = pCity;
        address = pAddress;
        zipcode = pZipcode;
    }

    public Address(Double pLongitude, Double pLatitude, String pCountry, String pCity, String pAddress, String pZipcode) {
        longitude = pLongitude;
        latitude = pLatitude;
        country = pCountry;
        city = pCity;
        address = pAddress;
        zipcode = pZipcode;
    }

    public Address(Double pLongitude, Double pLatitude, String pCountry, String pCity, String pAddress, String pZipcode, Boolean pIsPrimaryAddress) {
        longitude = pLongitude;
        latitude = pLatitude;
        country = pCountry;
        city = pCity;
        address = pAddress;
        zipcode = pZipcode;
        isPrimaryAddress = pIsPrimaryAddress;
    }
}
