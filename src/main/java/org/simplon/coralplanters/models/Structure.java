package org.simplon.coralplanters.models;

import jakarta.persistence.*;
import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "structure")
public class Structure {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idstructure", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "address_id", foreignKey = @ForeignKey(name = "fk_structure_address"))
    private Address address;

    public Structure(String pName, Address pAddress) {
        name = pName;
        address = pAddress;
    }
}