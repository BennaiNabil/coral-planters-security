package org.simplon.coralplanters.models;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Cascade;

import static org.hibernate.annotations.CascadeType.ALL;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "coral")
public class Coral {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcoral", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "nature", nullable = false)
    private String nature;

    @Column(name = "isparcel", nullable = false)
    private Boolean isparcel;

    @ManyToOne
    @JoinColumn(name = "address_id", foreignKey = @ForeignKey(name = "fk_coral_address"))
    @Cascade(ALL)
    private Address address;

    @ManyToOne
    @JoinColumn(name = "coral_parent_id", foreignKey = @ForeignKey(name = "fk_coral_parent"))
    private Coral coralParent;

    public Coral(String pName, String pDescription, String pNature, Boolean pIsparcel, Address pAddress, Coral pCoralParent) {
        name = pName;
        description = pDescription;
        nature = pNature;
        isparcel = pIsparcel;
        address = pAddress;
        coralParent = pCoralParent;
    }

    public Coral(String pName, String pDescription, String pNature, Boolean pIsparcel, Address pAddress) {
        name = pName;
        description = pDescription;
        nature = pNature;
        isparcel = pIsparcel;
        address = pAddress;
    }
}
