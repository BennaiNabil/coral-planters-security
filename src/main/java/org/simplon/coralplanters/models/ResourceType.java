package org.simplon.coralplanters.models;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "resourcetype")
public class ResourceType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idresourcetype", nullable = false)
    private Long id;

    @Column(name = "resourcetype", nullable = false)
    private String resourcetype;

    public ResourceType(String pResourcetype) {
        resourcetype = pResourcetype;
    }
}