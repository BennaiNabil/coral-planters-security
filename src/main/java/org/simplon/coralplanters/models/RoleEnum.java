package org.simplon.coralplanters.models;

public enum RoleEnum {
    ADMIN, USER, GUEST, GROUP;
}
