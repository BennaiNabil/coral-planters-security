package org.simplon.coralplanters.models;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Cascade;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "bankaccount")
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idbankaccount", nullable = false)
    private Long id;

    @Column(name = "bankname", nullable = false)
    private String bankname;

    @Column(name = "accountnumber", nullable = false)
    private String accountnumber;

    @Column(name = "ibancode", nullable = false)
    private String ibancode;

    @Column(name = "biccode", nullable = false)
    private String biccode;

    @ManyToOne
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "fk_bankaccount_user"))
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private User user;

    public BankAccount(String pBankname, String pAccountnumber, String pIbancode, String pBiccode, User pUser) {
        bankname = pBankname;
        accountnumber = pAccountnumber;
        ibancode = pIbancode;
        biccode = pBiccode;
        user = pUser;
    }
}
