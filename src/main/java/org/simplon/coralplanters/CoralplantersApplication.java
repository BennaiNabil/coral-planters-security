package org.simplon.coralplanters;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoralplantersApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoralplantersApplication.class, args);
    }

}
