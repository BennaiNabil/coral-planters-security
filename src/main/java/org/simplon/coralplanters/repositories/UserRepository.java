package org.simplon.coralplanters.repositories;

import org.simplon.coralplanters.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    @Query("select u from User u where u.lastname ilike ?1 or u.firstname ilike ?1")
    List<User> filterByName(String pName);

    @Query("select u from User u where u.email ilike ?1")
    Optional<User> findByEmail(String pEmail);
}