package org.simplon.coralplanters.repositories;

import org.simplon.coralplanters.models.Coral;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoralRepository extends JpaRepository<Coral, Long> {
}