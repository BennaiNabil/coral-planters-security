package org.simplon.coralplanters.repositories;

import org.simplon.coralplanters.models.Sponsorship;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SponsorshipRepository extends JpaRepository<Sponsorship, Long> {
}