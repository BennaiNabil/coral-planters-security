package org.simplon.coralplanters.repositories;

import org.simplon.coralplanters.models.Resource;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResourceRepository extends JpaRepository<Resource, Long> {
}