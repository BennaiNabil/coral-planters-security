package org.simplon.coralplanters.repositories;

import org.simplon.coralplanters.models.ResourceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ResourceTypeRepository extends JpaRepository<ResourceType, Long> {
    @Query("select r from ResourceType r where r.resourcetype ilike ?1")
    Optional<ResourceType> findByResourcetype(String pResourceTypeName);

    Optional<ResourceType> findByResourcetypeIgnoreCase(String resourceTypeName);
}
