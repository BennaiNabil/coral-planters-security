package org.simplon.coralplanters.repositories;

import org.simplon.coralplanters.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    @Query("select r from Role r where upper(r.role) = upper(?1)")
    Optional<Role> findByRoleIgnoreCase(String role);

    @Query("select r from Role r where r.role = ?1")
    Optional<Role> findByRole(String pRoleName);
}
