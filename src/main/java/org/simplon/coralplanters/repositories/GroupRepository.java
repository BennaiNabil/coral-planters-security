package org.simplon.coralplanters.repositories;

import org.simplon.coralplanters.models.Group;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<Group, Long> {
}