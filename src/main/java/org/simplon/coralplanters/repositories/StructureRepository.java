package org.simplon.coralplanters.repositories;

import org.simplon.coralplanters.models.Structure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StructureRepository extends JpaRepository<Structure, Long> {
}