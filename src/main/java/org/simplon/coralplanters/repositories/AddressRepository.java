package org.simplon.coralplanters.repositories;

import org.simplon.coralplanters.models.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
    @Query("select a from Address a where a.country ilike ?1")
    List<Address> findByCountry(String pCountry);

    @Query("select a from Address a where a.city ilike ?1")
    List<Address> findByCity(String pCity);
}