package org.simplon.coralplanters.controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface ICrudController<T> {
    public List<T> findAll();

    public T findById(@PathVariable Long id);

    public T update(@RequestBody T entity);

    public T save(@RequestBody T entity);

    public void deleteById(@PathVariable Long id);

    public void delete(T entity);

}
