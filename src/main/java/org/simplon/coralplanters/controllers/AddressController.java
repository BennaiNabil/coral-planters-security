package org.simplon.coralplanters.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.simplon.coralplanters.models.Address;
import org.simplon.coralplanters.services.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("api/address")
@Tag(name = "Address")
public class AddressController implements ICrudController<Address> {

    @Autowired
    private AddressService addressService;

    @GetMapping
    @ApiOperation(value = "Get all addresses", notes = "Returns a list of all addresses")
    public List<Address> findAll() {
        return this.addressService.findAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find address by ID", notes = "Returns a single address")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Address found"),
        @ApiResponse(code = 404, message = "Address not found")
    })
    public Address findById(@PathVariable Long id) {
        return this.addressService.findById(id);
    }

    @GetMapping("/country/{country}")
    @ApiOperation(value = "Find addresses by country", notes = "Returns a list of addresses for a given country")
    public List<Address> findByCountry(@PathVariable String country) {
        return this.addressService.findByCountry(country);
    }

    @GetMapping("/city/{city}")
    @ApiOperation(value = "Find addresses by city", notes = "Returns a list of addresses for a given city")
    public List<Address> findByCity(@PathVariable String city) {
        return this.addressService.findByCity(city);
    }

    @PutMapping
    @ApiOperation(value = "Update an address", notes = "Updates an existing address")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Address updated"),
        @ApiResponse(code = 404, message = "Address not found")
    })
    public Address update(@RequestBody Address entity) {
        return this.addressService.save(entity);
    }

    @PostMapping
    @ApiOperation(value = "Create an address", notes = "Creates a new address")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Address created"),
        @ApiResponse(code = 400, message = "Invalid input")
    })
    public Address save(@RequestBody Address entity) {
        return this.addressService.save(entity);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete an address", notes = "Deletes an address by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Address deleted"),
        @ApiResponse(code = 404, message = "Address not found")
    })
    public void deleteById(@PathVariable Long id) {
        this.addressService.deleteById(id);
    }

    @Override
    @DeleteMapping
    @ApiOperation(value = "Delete an address", notes = "Deletes an address")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Address deleted"),
        @ApiResponse(code = 404, message = "Address not found")
    })
    public void delete(@RequestBody Address entity) {
        this.addressService.delete(entity);
    }
}
