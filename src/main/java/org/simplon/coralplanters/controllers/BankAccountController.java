package org.simplon.coralplanters.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.simplon.coralplanters.models.BankAccount;
import org.simplon.coralplanters.services.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/bankaccount")
@Tag(name = "BankAccount")
public class BankAccountController implements ICrudController<BankAccount> {

    @Autowired
    private BankAccountService bankAccountService;

    @GetMapping
    @ApiOperation(value = "Get all bank accounts", notes = "Returns a list of all bank accounts")
    public List<BankAccount> findAll() {
        return this.bankAccountService.findAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find bank account by ID", notes = "Returns a single bank account")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Bank account found"),
        @ApiResponse(code = 404, message = "Bank account not found")
    })
    public BankAccount findById(@PathVariable Long id) {
        return this.bankAccountService.findById(id);
    }

    @GetMapping("/user/{userId}")
    @ApiOperation(value = "Find bank accounts by user ID", notes = "Returns a list of bank accounts for a given user")
    public List<BankAccount> findByUserId(@PathVariable Long userId) {
        return this.bankAccountService.findByUserId(userId);
    }

    @PutMapping
    @ApiOperation(value = "Update a bank account", notes = "Updates an existing bank account")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Bank account updated"),
        @ApiResponse(code = 404, message = "Bank account not found")
    })
    public BankAccount update(@RequestBody BankAccount entity) {
        return this.bankAccountService.save(entity);
    }

    @PostMapping
    @ApiOperation(value = "Create a bank account", notes = "Creates a new bank account")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Bank account created"),
        @ApiResponse(code = 400, message = "Invalid input")
    })
    public BankAccount save(@RequestBody BankAccount entity) {
        return this.bankAccountService.save(entity);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a bank account", notes = "Deletes a bank account by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Bank account deleted"),
        @ApiResponse(code = 404, message = "Bank account not found")
    })
    public void deleteById(@PathVariable Long id) {
        this.bankAccountService.deleteById(id);
    }

    @Override
    @DeleteMapping
    @ApiOperation(value = "Delete a bank account", notes = "Deletes a bank account")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Bank account deleted"),
        @ApiResponse(code = 404, message = "Bank account not found")
    })
    public void delete(@RequestBody BankAccount entity) {
        this.bankAccountService.delete(entity);
    }
}
