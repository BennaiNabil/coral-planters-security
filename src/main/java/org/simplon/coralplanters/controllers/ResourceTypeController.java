package org.simplon.coralplanters.controllers;

import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.simplon.coralplanters.models.ResourceType;
import org.simplon.coralplanters.services.ResourceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/resourcetype")
@Tag(name = "ResourceType")
public class ResourceTypeController implements ICrudController<ResourceType> {

    @Autowired
    private ResourceTypeService resourceTypeService;

    @GetMapping
    @ApiOperation(value = "Get all resource types", notes = "Returns a list of all resource types")
    public List<ResourceType> findAll() {
        return this.resourceTypeService.findAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find resource type by ID", notes = "Returns a single resource type")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Resource type found"),
        @ApiResponse(code = 404, message = "Resource type not found")
    })
    public ResourceType findById(@PathVariable Long id) {
        return this.resourceTypeService.findById(id);
    }

    @GetMapping("/name/{resourceTypeName}")
    @ApiOperation(value = "Find resource type by name", notes = "Returns a single resource type")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Resource type found"),
        @ApiResponse(code = 404, message = "Resource type not found")
    })
    public ResourceType findByResourceTypeName(@PathVariable String resourceTypeName) {
        return this.resourceTypeService.findByResourcetype(resourceTypeName);
    }

    @PutMapping
    @ApiOperation(value = "Update a resource type", notes = "Updates an existing resource type")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Resource type updated"),
        @ApiResponse(code = 404, message = "Resource type not found")
    })
    public ResourceType update(@RequestBody ResourceType entity) {
        return this.resourceTypeService.save(entity);
    }

    @PostMapping
    @ApiOperation(value = "Create a resource type", notes = "Creates a new resource type")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Resource type created"),
        @ApiResponse(code = 400, message = "Invalid input")
    })
    public ResourceType save(@RequestBody ResourceType entity) {
        return this.resourceTypeService.save(entity);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a resource type by ID", notes = "Deletes a resource type by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Resource type deleted"),
        @ApiResponse(code = 404, message = "Resource type not found")
    })
    public void deleteById(@PathVariable Long id) {
        this.resourceTypeService.deleteById(id);
    }

    @Override
    @DeleteMapping
    @ApiOperation(value = "Delete a resource type", notes = "Deletes a resource type")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Resource type deleted"),
        @ApiResponse(code = 404, message = "Resource type not found")
    })
    public void delete(@RequestBody ResourceType entity) {
        this.resourceTypeService.delete(entity);
    }

}
