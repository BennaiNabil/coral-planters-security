package org.simplon.coralplanters.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.simplon.coralplanters.models.User;
import org.simplon.coralplanters.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("api/user")
@Slf4j
@Tag(name = "User")
public class UserController implements ICrudController<User> {

    @Autowired
    private UserService userService;

    @GetMapping
    @ApiOperation(value = "Get all users", notes = "Returns a list of all users")
    public List<User> findAll() {
        log.info("GET api/user/ - Find all users");
        return this.userService.findAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find user by ID", notes = "Returns a single user")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User found"),
        @ApiResponse(code = 404, message = "User not found")
    })
    public User findById(@PathVariable Long id) {
        log.info("GET api/user/{} - Find user by id", id);
        return this.userService.findById(id);
    }

    @GetMapping("/name/{name}")
    @ApiOperation(value = "Find users by name", notes = "Returns a list of users with a matching name")
    public List<User> filterByName(@PathVariable String name) {
        log.info("GET api/user/name/{} - Find user by name", name);
        return this.userService.filterByName(name);
    }

    @GetMapping("/email/{email}")
    @ApiOperation(value = "Find user by email", notes = "Returns a single user with a matching email")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User found"),
        @ApiResponse(code = 404, message = "User not found")
    })
    public User findByEmail(@PathVariable String email) {
        log.info("GET api/user/email/{} - Find user by email", email);
        return this.userService.findByEmail(email);
    }

    @PutMapping
    @ApiOperation(value = "Update a user", notes = "Updates an existing user")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User updated"),
        @ApiResponse(code = 404, message = "User not found")
    })
    public User update(@RequestBody User entity) {
        log.info("PUT api/user/ - Update user");
        return this.userService.save(entity);
    }

    @PostMapping
    @ApiOperation(value = "Create a user", notes = "Creates a new user")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "User created"),
        @ApiResponse(code = 400, message = "Invalid input")
    })
    public User save(@RequestBody User entity) {
        log.info("POST api/user/ - Save user");
        entity.setCreatedAt(LocalDateTime.now());
        return this.userService.save(entity);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a user by ID", notes = "Deletes a user by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User deleted"),
        @ApiResponse(code = 404, message = "User not found")
    })
    public void deleteById(@PathVariable Long id) {
        log.info("DELETE api/user/{} - Delete user by id", id);
        this.userService.deleteById(id);
    }

    @Override
    @ApiOperation(value = "Delete a user", notes = "Deletes a user")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User deleted"),
        @ApiResponse(code = 404, message = "User not found")
    })
    public void delete(User entity) {
        log.info("DELETE api/user/ - Delete user");
        this.userService.delete(entity);
    }
}
