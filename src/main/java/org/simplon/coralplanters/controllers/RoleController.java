package org.simplon.coralplanters.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.simplon.coralplanters.models.Role;
import org.simplon.coralplanters.models.RoleEnum;
import org.simplon.coralplanters.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/role")
@CrossOrigin(origins = "http://localhost:4200")
@Tag(name = "Role")
public class RoleController implements ICrudController<Role> {

    @Autowired
    private RoleService service;

    @GetMapping
    @ApiOperation(value = "Get all roles", notes = "Returns a list of all roles")
    public List<Role> findAll() {
        return this.service.findAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find role by ID", notes = "Returns a single role")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Role found"), @ApiResponse(code = 404, message = "Role not found")})
    public Role findById(@PathVariable Long id) {
        return this.service.findById(id);
    }

    @GetMapping("/name/{roleName}")
    @ApiOperation(value = "Find role by name", notes = "Returns a single role")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Role found"), @ApiResponse(code = 404, message = "Role not found")})
    public Role findByRoleName(@PathVariable String roleName) {
        return this.service.findByRole(RoleEnum.valueOf(roleName));
    }

    @PutMapping
    @ApiOperation(value = "Update a role", notes = "Updates an existing role")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Role updated"), @ApiResponse(code = 404, message = "Role not found")})
    public Role update(@RequestBody Role entity) {
        return this.service.save(entity);
    }

    @PostMapping
    @ApiOperation(value = "Create a role", notes = "Creates a new role")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Role created"), @ApiResponse(code = 400, message = "Invalid input")})
    public Role save(@RequestBody Role entity) {
        return this.service.save(entity);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a role by ID", notes = "Deletes a role by ID")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Role deleted"), @ApiResponse(code = 404, message = "Role not found")})
    public void deleteById(@PathVariable Long id) {
        this.service.deleteById(id);
    }

    @Override
    @DeleteMapping
    @ApiOperation(value = "Delete a role", notes = "Deletes a role")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Role deleted"), @ApiResponse(code = 404, message = "Role not found")})
    public void delete(@RequestBody Role entity) {
        this.service.delete(entity);
    }

}
