package org.simplon.coralplanters.config;

import com.github.javafaker.Faker;
import org.simplon.coralplanters.models.*;
import org.simplon.coralplanters.repositories.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

//@Configuration
public class AppFixtures implements CommandLineRunner {


    private final AddressRepository addressRepository;
    private final BankAccountRepository bankAccountRepository;
    private final CoralRepository coralRepository;
    private final ResourceTypeRepository resourceTypeRepository;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final ResourceRepository resourceRepository;
    private final StructureRepository structureRepository;
    private final GroupRepository groupRepository;
    private final SponsorshipRepository sponsorshipRepository;


    public AppFixtures(RoleRepository roleRepository, AddressRepository addressRepository, UserRepository userRepository, BankAccountRepository bankAccountRepository, ResourceTypeRepository resourceTypeRepository, CoralRepository coralRepository, ResourceRepository resourceRepository, StructureRepository structureRepository, GroupRepository groupRepository, SponsorshipRepository sponsorshipRepository) {
        this.roleRepository = roleRepository;
        this.addressRepository = addressRepository;
        this.userRepository = userRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.resourceTypeRepository = resourceTypeRepository;
        this.coralRepository = coralRepository;
        this.resourceRepository = resourceRepository;
        this.structureRepository = structureRepository;
        this.groupRepository = groupRepository;
        this.sponsorshipRepository = sponsorshipRepository;
    }

    public static LocalDateTime toLocalDateTime(Date date) {
        Instant instant = date.toInstant();
        ZoneId zoneId = ZoneId.systemDefault();
        return instant.atZone(zoneId).toLocalDateTime();
    }


    @Override
    public void run(String... args) throws Exception {

        final int NB_USERS = 100;
        final int NB_ADDRESSES = 100;
        final int NB_BANKACCOUNTS = 100;
        final int NB_CORALS = 500;
        final int RESOURCES_PER_CORAL = 3;
        final int NB_STRUCTURES = 50;

        // Faking data with Faker library, using french locale
        Faker faker = new Faker(new Locale("fr"));

        // Inserting roles
        List<Role> roles = List.of(new Role(RoleEnum.USER), new Role(RoleEnum.ADMIN), new Role(RoleEnum.GUEST));
        roleRepository.saveAll(roles);

        // Faking addresses
        List<Address> addresses = new ArrayList<>();
        for (int i = 0; i < NB_ADDRESSES; i++) {
            addresses.add(new Address(Double.valueOf(faker.address().longitude()
                    .replace(",", ".")), Double.valueOf(faker.address().latitude().replace(",", ".")), faker.address()
                    .country(), faker.address().city(), faker.address().streetAddress(), faker.address()
                    .zipCode(), faker.bool().bool()));
        }
        addressRepository.saveAll(addresses);

        // Faking Users
        List<User> users = new ArrayList<>();
        for (int i = 0; i < NB_USERS; i++) {
            List<Address> tmpAddresses = new ArrayList<>();
            int tmpNbAddresses = faker.number().numberBetween(1, 4);
            for (int j = 0; j < tmpNbAddresses; j++) {
                tmpAddresses.add(addresses.get(faker.number().numberBetween(0, addresses.size() - 1)));
            }
            users.add(new User(faker.name().firstName(), faker.name().lastName(), faker.internet()
                    .emailAddress(), faker.internet().password(), toLocalDateTime(faker.date().birthday()), faker.bool()
                    .bool() ? null : toLocalDateTime(faker.date().birthday()), faker.phoneNumber()
                    .phoneNumber(), roles.get(faker.number().numberBetween(0, roles.size() - 1)), tmpAddresses));
        }
        userRepository.saveAll(users);


        // Faking BankAccounts
        List<BankAccount> bankAccounts = new ArrayList<>();
        for (int i = 0; i < NB_BANKACCOUNTS; i++) {
            bankAccounts.add(new BankAccount(faker.company().name(), faker.finance().creditCard(), faker.finance()
                    .iban(), faker.finance().bic(), users.get(faker.number().numberBetween(0, users.size() - 1))));
        }
        bankAccountRepository.saveAll(bankAccounts);

        // Adding resource types
        List<ResourceType> resourceTypes = List.of(new ResourceType("Image"), new ResourceType("Video"), new ResourceType("Text"), new ResourceType("Audio"));
        resourceTypeRepository.saveAll(resourceTypes);

        // Faking Corals
        List<Coral> corals = new ArrayList<>();
        for (int i = 0; i < NB_CORALS; i++) {
            Coral tmpCoral = new Coral(faker.name().name(), faker.lorem().paragraph(1), faker.lorem()
                    .paragraph(1), faker.bool().bool(), addresses.get(faker.number()
                    .numberBetween(0, addresses.size() - 1)));
            if (tmpCoral.getIsparcel() && !corals.isEmpty()) {
                tmpCoral.setCoralParent(corals.get(faker.number().numberBetween(0, corals.size() - 1)));
            }
            corals.add(tmpCoral);
        }
        coralRepository.saveAll(corals);

        // Faking Resources
        List<Resource> resources = new ArrayList<>();
        for (int idxCoral = 0; idxCoral < NB_CORALS; idxCoral++) {
            for (int idxRes = 0; idxRes < RESOURCES_PER_CORAL; idxRes++) {
                Resource tmpResource = new Resource(faker.internet().url(), toLocalDateTime(faker.date()
                        .birthday()), resourceTypes.get(faker.number()
                        .numberBetween(0, resourceTypes.size() - 1)), corals.get(idxCoral));
                resources.add(tmpResource);
            }
        }
        resourceRepository.saveAll(resources);

        // Faking Structures
        List<Structure> structures = new ArrayList<>();
        for (int i = 0; i < NB_STRUCTURES; i++) {
            structures.add(new Structure(faker.commerce().productName(), addresses.get(faker.number()
                    .numberBetween(0, addresses.size() - 1))));
        }
        structureRepository.saveAll(structures);

        // Faking Groups
        List<Group> groups = new ArrayList<>();
        for (int i = 0; i < NB_STRUCTURES; i++) {
            List<User> tmpUsers = new ArrayList<>();
            int tmpNbUsers = faker.number().numberBetween(1, 10);
            for (int j = 0; j < tmpNbUsers; j++) {
                tmpUsers.add(users.get(faker.number().numberBetween(0, users.size() - 1)));
            }
            groups.add(new Group(faker.commerce().productName(), toLocalDateTime(faker.date().birthday()), faker.bool()
                    .bool() ? null : toLocalDateTime(faker.date().birthday()), users.get(faker.number()
                    .numberBetween(0, users.size() - 1)), users.get(faker.number()
                    .numberBetween(0, users.size() - 1)), structures.get(faker.number()
                    .numberBetween(0, structures.size() - 1)), tmpUsers));
        }
        groupRepository.saveAll(groups);

        // Faking Sponsorships
        List<Sponsorship> sponsorships = new ArrayList<>();
        for (int i = 0; i < NB_CORALS; i++) {
            Coral tmpCoral = corals.get(i);
            if (faker.number().numberBetween(0, 5) == 1 && tmpCoral.getIsparcel()) {
                sponsorships.add(new Sponsorship(faker.number().numberBetween(1, 10000), toLocalDateTime(faker.date()
                        .birthday()), users.get(faker.number().numberBetween(0, users.size() - 1)), tmpCoral));
            }
        }
        sponsorshipRepository.saveAll(sponsorships);
    }
}
