#!/bin/bash

# Build the application
./mvnw clean install

# Run the tests and generate a code coverage report
mvn test jacoco:report

# Open the HTML report in the default web browser
open target/site/jacoco/index.html
